"""Bulk copying of geo data to parsing directory"""
import os
import shutil
import time
#
GEOSTORAGE_PATH = './repo/stash/ingest.processbyday'
if not os.path.exists(GEOSTORAGE_PATH):
    print("path not found: %s" % GEOSTORAGE_PATH)
    exit
PARSING_PATH = './repo/ingest/processfileswithinsert'
if not os.path.exists(PARSING_PATH):
    print("path not found: %s" % PARSING_PATH)
    exit
if not os.path.exists('%s/buffer' % PARSING_PATH):
	os.makedirs('%s/buffer' % PARSING_PATH)

year = '2013'
month = '10'
workdir = '%s/%s/%s' % (GEOSTORAGE_PATH, year, month)
tag = 'OC'

for f in os.listdir(workdir):
    path = os.path.join(workdir, f)
    if os.path.isdir(path):
        for tagDir in os.listdir(path):
            if tagDir == tag:
                tagDirPath = os.path.join(path, tagDir)
                # Wait for possible previouse files operations
                print('tag dir %s' % (tagDirPath))
                time.sleep(10)
                for fyle in os.listdir(tagDirPath):
                    srcFile = '%s/%s' % tagDirPath, fyle
                    destFile = '%s/buffer' % PARSING_PATH
                    # destLink = '%s/buffer/%s' % (PARSING_PATH, fyle)
                    # print('src %s - dest %s' % (srcFile, destLink))
                    os.symlink(srcFile, destFile)
                    # shutil.copy(srcFile, destFile)
                # Waiting for poissible disk cache operations
                time.sleep(2)
                # Move copied data for parsing
                for fyle in os.listdir('%s/buffer' % PARSING_PATH):
                    srcFile = os.path.join('%s/buffer' % PARSING_PATH, fyle)
                    destFile = os.path.join(PARSING_PATH, fyle)
                    # print('src %s - dest %s' % (srcFile, destFile))
                    os.rename(srcFile, destFile)
