import React from 'react'
import styled from 'styled-components'
//
import { THRASHER } from '../../constants'
import { randColor } from '../../helpers'
import Panel from './Panel'
import Map from './Map'
import Points from './Points'


const StyledDataPoints = styled.div`
  background-color: slateblue;
  overflow-y: hidden;
`

export default class DataPoints extends React.Component {
  state = {
    data: [],
  }

  componentDidMount = () => {
    fetch(`${THRASHER}/ocean/testdatapoints/`)
      .then(res => res.json())
      .then(json => {
        const directories = []
        let fileCounter = 0
        json.forEach(dir => {
          const thisdatafiles = dir.datafiles.map(file => {
            // Increase file counter
            fileCounter += 1
            return {
              ...file,
              // Add fileID file structure
              fileID: fileCounter,
              // Add checkbox status
              checked: true,
              // Get random color for points
              color: randColor(),
            }
          })
          directories.push({
            datafiles: thisdatafiles,
            dirname: dir.dirname,
          })
        })
        // Update component state
        this.setState({ data: directories })
      })
  }

  setFileCheck = (id, checked) => {
    const thisdata = this.state.data
    thisdata.forEach(dir => {
      dir.datafiles.forEach(file => {
        // Set file checked flag if file ID suites
        if (id === file.fileID) {
          file.checked = checked
        }
      })
    })
    this.setState({ data: thisdata })
  }

  render () {
    return (
      <StyledDataPoints>
        <Panel
          data={this.state.data}
          _setFileCheck={this.setFileCheck}
        />
        <Map>
          <Points data={this.state.data} />
        </Map>
      </StyledDataPoints>
    )
  }
}
