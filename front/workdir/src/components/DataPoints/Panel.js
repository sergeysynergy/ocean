import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
//
import CheckBar from '../includes/HandyBar/CheckBar'


const StyledPanel = styled.div`
  // border: dashed 1px red;
  position: absolute;
  top: 42px;
  padding: .2em 0 .8em 0;
  margin: 0;
  width: 300px;
  // overflow-y: scroll;
  background-color: gray;
  color: white;

  h2 {
    padding: .4em .6em .2em .6em;
    color: white;
    font-size: 1.2em;
  }
`

const StyledDirectory = styled.div`
  // border: dashed 1px red;
  margin: .2em 0;
  padding: .1em .6em .6em .6em;
  background-color: darkslateblue;
  color: white;
  h2 {
    color: white;
    font-size: 1.2em;
    margin: 0 0 .4em 0;
    padding: .4em 0;
    border-bottom: dashed 1px slateblue;
  }
`

const StyledFile = styled.div`
  // border: dashed 1px red;
  color: white;
  padding: .1em 0;
`


export default class Panel extends React.Component {
  render () {
    return (
      <StyledPanel>
        <h2>Массивы точек геоданных</h2>
        <Directories
          data={this.props.data}
          _setFileCheck={this.props._setFileCheck}
        />
      </StyledPanel>
    )
  }
}


class Directories extends React.Component {
  // Render controls for all data directories
  render () {
    const directories = []
    this.props.data.forEach((dir, key) => {
      directories.push(
        <Directory
          key={key}
          dirname={dir.dirname}
          data={dir.datafiles}
          _setFileCheck={this.props._setFileCheck}
        />
      )
    })
    return (
      <div>
        {directories}
      </div>
    )
  }
}


class Directory extends React.Component {
  // Render controls for all data directories
  render () {
    const files = []
    this.props.data.forEach(file => {
      files.push(
        <File
          key={file.fileID}
          fileID={file.fileID}
          checked={file.checked}
          filename={file.filename}
          _setFileCheck={this.props._setFileCheck}
        />
      )
    })
    return (
      <StyledDirectory>
        <h2>{this.props.dirname}</h2>
        {files}
      </StyledDirectory>
    )
  }
}


class File extends React.Component {
  static propTypes = {
    _setFileCheck: PropTypes.func.isRequired,
  }
  // Render controls for all data directories
  handleCheck = (checked, id) => {
    this.props._setFileCheck(id, checked)
  }

  render () {
    return (
      <StyledFile>
        <CheckBar
          id={this.props.fileID}
          checked={this.props.checked}
          lable={this.props.filename}
          _handleCheck={this.handleCheck}
        />
      </StyledFile>
    )
  }
}
