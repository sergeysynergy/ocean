import React from 'react'
import PropTypes from 'prop-types'
import {
  FormGroup,
  Label,
  Input,
} from 'reactstrap'

import './ZBootstrap.css'


export class ZSelect extends React.Component {
  static propTypes = {
    handleStateKeyValueUpdate: PropTypes.func,
  }
  constructor (props) {
    super(props)
    this.options = []

    // Forming options list array
    this.props.options.forEach((element, key) => {
      this.options.push(
        <option
          key={key}
          value={element.value}
          selected={element.selected}
        >
          {element.value}
        </option>
      )
    })
  }

  handleChange = obj => {
    // Buble date changes to the parent component
    this.props.handleStateKeyValueUpdate(this.props.name, obj.target.value)
  }

  render = () => (
    <FormGroup>
      <Label for="formControlsSelect">{this.props.title}</Label>
      <Input
        type="select"
        name="select"
        id="formControlsSelect"
        defaultValue={this.props.defaultValue}
        value={this.props.value}
        placeholder="select"
        onChange={this.handleChange}
      >
        {this.options}
      </Input>
    </FormGroup>
  )
}


export class ZCheckbox extends React.Component {
  static propTypes = {
    handleStateKeyValueUpdate: PropTypes.func,
  }
  handleChange = obj => {
    const options = this.props.options
    const find = value => options.forEach((element, index, array) => {
      if (element.value === value) {
        array[index].checked = !array[index].checked
      }
    })
    find(obj.target.value)
    this.props.handleStateKeyValueUpdate('characteristics', options)
  }

  render = () => {
    // Forming checkbox list array
    const rows = []
    if (typeof this.props.options !== 'undefined'
        && this.props.options.length > 0
    ) {
      // console.log('type', this.props)
      this.props.options.forEach((element, key) => {
        rows.push(
          <Label check key={key} className="ZCheckbox">
            <Input
              type="checkbox"
              id={key}
              value={element.value}
              onChange={this.handleChange}
              checked={element.checked}
            />
            {element.label}
          </Label>
        )
      })
    }

    return (
      <div>
        <Label>{this.props.title}</Label>
        <FormGroup check>
          {rows}
        </FormGroup>
      </div>
    )
  }
}

/*
export class ZFieldGroup extends React.Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(obj) {
    let func = this.props._handleChange
    if (typeof func === 'function') {
      func(this.props.id, obj.target.value)
    }
  }

  render() {
    return (
      <FormGroup controlId={this.props.id}>
        <ControlLabel>{this.props.label}</ControlLabel>
        <FormControl
          value={this.props.value}
          type={this.props.type}
          placeholder={this.props.placeholder}
          onChange={this.handleChange}
        />
        {this.props.help && <HelpBlock>{this.props.help}</HelpBlock>}
      </FormGroup>
    )
  }
}


export class ZSingleCheckbox extends React.Component {
  constructor(props) {
    super(props)
    this.options = []
    this.state = {
      checked: this.props.checked,
    }
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(obj) {
    let func = this.props._handleChange
    if (typeof func === 'function') {
      func(this.props.id, obj.target.checked)
    }
  }

  render() {
    return (
      <Checkbox
        key={this.props.id}
        checked={this.props.checked}
        onChange={this.handleChange}
      >
        {this.props.lable}
      </Checkbox>
    )
  }
}


export class ZText extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      value: '',
    }
    this.getValidationState = this.getValidationState.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(obj) {
    let func = this.props._handleChange
    if (typeof func === 'function') {
      func(this.props.name, obj.target.value)
    }
    this.setState({ value: obj.target.value })
  }
  getValidationState() {
    const length = this.state.value.length;
    if (length > 10) return 'success';
    else if (length > 5) return 'warning';
    else if (length > 0) return 'error';
  }

  render() {
    return (
      <FormGroup
        controlId={this.props.name}
        // validationState={this.getValidationState()}
      >
        <ControlLabel>{this.props.label}</ControlLabel>
        <FormControl
          type="text"
          value={this.state.value}
          placeholder="Введите текст"
          onChange={this.handleChange}
        />
        <FormControl.Feedback />
        <HelpBlock>{this.props.help}</HelpBlock>
      </FormGroup>
    )
  }
}


const FormExample = React.createClass({
  render() {
    return (
      <form>
      </form>
    );
  }
});
*/
