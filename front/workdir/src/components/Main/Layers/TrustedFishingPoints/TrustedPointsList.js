import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'
//
import environment from '../../../../Environment'
import TrustedPointsListItem from './TrustedPointsListItem'
//
const query = graphql`
  query TrustedPointsListQuery ($q: String!) {
    bulkTrusted(q: $q) {
      ...TrustedPointsListItem_point
    }
  }
`


export default class Points extends Component {
  render () {
    return (
      <QueryRenderer
        environment={environment}
        query={query}
        variables={{
          q: this.props.dateFilter,
        }}
        render={({ error, props }) => {
          if (error) {
            return <div>{error.message}</div>
          } else if (props) {
            // console.log('props', props);

            return (
              <div>
                {this.props.visible &&
                  props.bulkTrusted.map(node => (
                    <TrustedPointsListItem
                      key={node.__id}
                      point={node}
                    />
                  ))
                }
              </div>
            )
          }
          return <div>Loading</div>
        }}
      />
    )
  }
}
