/**
 * @flow
 * @relayHash 7b7a11a8efc9cfecd610c1a0c4b4641f
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type TrustedPointsListItem_point$ref = any;
export type TrustedPointsListQueryVariables = {|
  q: string,
|};
export type TrustedPointsListQueryResponse = {|
  +bulkTrusted: ?$ReadOnlyArray<?{|
    +$fragmentRefs: TrustedPointsListItem_point$ref,
  |}>,
|};
*/


/*
query TrustedPointsListQuery(
  $q: String!
) {
  bulkTrusted(q: $q) {
    ...TrustedPointsListItem_point
    id
  }
}

fragment TrustedPointsListItem_point on TrustedNode {
  id
  lat
  lon
  datetime
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "q",
    "type": "String!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "q",
    "variableName": "q",
    "type": "String"
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "TrustedPointsListQuery",
  "id": null,
  "text": "query TrustedPointsListQuery(\n  $q: String!\n) {\n  bulkTrusted(q: $q) {\n    ...TrustedPointsListItem_point\n    id\n  }\n}\n\nfragment TrustedPointsListItem_point on TrustedNode {\n  id\n  lat\n  lon\n  datetime\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "TrustedPointsListQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "bulkTrusted",
        "storageKey": null,
        "args": v1,
        "concreteType": "TrustedNode",
        "plural": true,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "TrustedPointsListItem_point",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "TrustedPointsListQuery",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "bulkTrusted",
        "storageKey": null,
        "args": v1,
        "concreteType": "TrustedNode",
        "plural": true,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "lat",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "lon",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "datetime",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
};
})();
(node/*: any*/).hash = '24ae5227309f8f62a242f9baaa6d7cbc';
module.exports = node;
