import React, { Component } from 'react'
import PropTypes from 'prop-types'
// import fetch from 'node-fetch'
//
// import { THRASHER } from '../../../../constants'
//
const isBrowser = typeof window !== 'undefined'
const RL = isBrowser ? require('react-leaflet') : undefined
//

export default class HullLayer extends Component {
  static propTypes = {
    visible: PropTypes.bool,
    hulls: PropTypes.array,
  }
  static defaultProps = {
    visible: true,
    hulls: [],
  }

  render = () => {
    const hulls = this.props.hulls.map(hull =>
      <Hull hull={hull} key={hull.id} />
    )

    return (
      <div>
        {this.props.visible &&
          <div>
            {hulls}
          </div>
        }
      </div>
    )
  }
}

export class Hull extends Component {
  static propTypes = {
    hull: PropTypes.object,
  }
  static defaultProps = {
    hull: {},
  }

  render = () => {
    const item = this.props.hull

    return (
      <div>
        {item.visible &&
          <RL.Polygon
            positions={item.hull}
            color="#CC00CC"
          />
        }
      </div>
    )
  }
}
