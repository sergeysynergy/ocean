import React, { Component } from 'react'
import PropTypes from 'prop-types'
//
import CheckBar from '../../../includes/HandyBar/CheckBar'
import './style.css'


export default class HullControls extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    _setLayer: PropTypes.func.isRequired,
    _setHullDir: PropTypes.func.isRequired,
    _setHull: PropTypes.func.isRequired,
    checked: PropTypes.bool,
    hulls: PropTypes.array,
  }
  static defaultProps = {
    checked: true,
    hulls: [],
  }

  handleCheck = checked => {
    this.props._setLayer(this.props.name, checked)
  }

  handleHullDirCheck = (checked, id) => {
    this.props._setHullDir(id, checked)
  }

  handleHullCheck = (checked, id) => {
    this.props._setHull(id, checked)
  }

  render = () => {
    const directories = this.props.hulls.map(dir => (
      <div key={dir.id}>
        <CheckBar
          id={dir.id}
          checked={dir.visible}
          _handleCheck={this.handleHullDirCheck}
          lable={dir.dirname}
        />
        {
          dir.visible && dir.metafiles.map(file => (
            <div className="files" key={file.id}>
              <CheckBar
                id={file.id}
                checked={file.visible}
                _handleCheck={this.handleHullCheck}
                lable={file.filename}
              />
            </div>
          ))
        }
      </div>
    ))

    return (
      <div className="Hull">
        <h5>
          <CheckBar
            key="hulls"
            checked={this.props.checked}
            _handleCheck={this.handleCheck}
            lable="Границы кластеризированных данных"
          />
        </h5>
        {this.props.checked &&
          <div>
            {directories}
          </div>
        }
      </div>
    )
  }
}
