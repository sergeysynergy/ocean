import React, { Component } from 'react'
import PropTypes from 'prop-types'
//
import './ChlorophyllTest.css'
import CheckBar from '../../includes/HandyBar/CheckBar'
//
const isBrowser = typeof window !== 'undefined'
const RL = isBrowser ? require('react-leaflet') : undefined


export class ChlorophyllTestControls extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    _setLayer: PropTypes.func.isRequired,
    checked: PropTypes.bool,
  }
  static defaultProps = {
    checked: true,
  }

  handleCheck = checked => {
    this.props._setLayer(this.props.name, checked)
  }

  render = () => (
    <div className="ChlorophyllTest">
      <CheckBar
        checked={this.props.checked}
        _handleCheck={this.handleCheck}
        lable="Данные MODIS из файлов .nc: в многоугольнике 2745237 точек"
      />
    </div>
  )
}

export default class ChlorophyllTestLayer extends Component {
  static defaultProps = {
    visible: true,
  }

  // 57, 139.5
  // 35.3, 164.1
  state = {
    data: [
      [
        [57, 139.5],
        [39.1, 138.2],
        [35.3, 164.1],
        [51.9, 175.1],
        [57, 139.5],
      ],
      [
        [69.1, -21.5],
        [86.3, 10.4],
        [71.1, -112.6],
        [62, -71.3],
        [69.1, -21.5],
      ],
    ],
    // visible: this.props.visible,
  }

  render = () => {
    // console.log('state', this.state.data)
    console.log('')
    return (
      <div>
        {this.props.visible &&
        <RL.Polygon
          positions={this.state.data}
          color="lightgreen"
        />
      }
      </div>
    )
  }
}
