/**
 * @flow
 * @relayHash 75b818d8ac46058e928cbb7edfc6101a
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type ExpectedItem_point$ref = any;
export type ExpectedLayerQueryVariables = {|
  q: string,
|};
export type ExpectedLayerQueryResponse = {|
  +bulkExpected: ?$ReadOnlyArray<?{|
    +$fragmentRefs: ExpectedItem_point$ref,
  |}>,
|};
*/


/*
query ExpectedLayerQuery(
  $q: String!
) {
  bulkExpected(q: $q) {
    ...ExpectedItem_point
    id
  }
}

fragment ExpectedItem_point on ExpectedNode {
  id
  lat
  lon
  datetime
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "q",
    "type": "String!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "q",
    "variableName": "q",
    "type": "String"
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "ExpectedLayerQuery",
  "id": null,
  "text": "query ExpectedLayerQuery(\n  $q: String!\n) {\n  bulkExpected(q: $q) {\n    ...ExpectedItem_point\n    id\n  }\n}\n\nfragment ExpectedItem_point on ExpectedNode {\n  id\n  lat\n  lon\n  datetime\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "ExpectedLayerQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "bulkExpected",
        "storageKey": null,
        "args": v1,
        "concreteType": "ExpectedNode",
        "plural": true,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "ExpectedItem_point",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "ExpectedLayerQuery",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "bulkExpected",
        "storageKey": null,
        "args": v1,
        "concreteType": "ExpectedNode",
        "plural": true,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "lat",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "lon",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "datetime",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
};
})();
(node/*: any*/).hash = 'd50c40ff8d183c1600e3b9d88b47e221';
module.exports = node;
