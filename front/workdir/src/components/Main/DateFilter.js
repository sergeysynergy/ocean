import React, { Component } from 'react'
import { Button } from 'reactstrap'
import YearMonthSelector from 'react-year-month-selector'
//
import './DateFilter.css'
import '../../css/YearMonthSelector.css'


export default class DateFilter extends Component {
  state = {
    open: true,
    from: {
      year: 2015,
      month: 1,
      open: false,
    },
    till: {
      year: 2015,
      month: 12,
      open: false,
    },
    collapsed: true,
  }

  handleClick = (entity1, entity2) => {
    const state1 = this.state[entity1]
    state1.open = !state1.open

    const state2 = this.state[entity2]
    state2.open = false

    this.setState({ [entity1]: state1, [entity2]: state2 })
  }

  handleFromChange = (year, month) => {
    const from = this.state.from
    const till = this.state.till
    from.year = year
    from.month = month
    const dateFilter = `${from.year}-${from.month}/${till.year}-${till.month}`
    this.props._handleUpdate(dateFilter)
    this.setState({ from: { from } })
  }

  handleTillChange = (year, month) => {
    const from = this.state.from
    const till = this.state.till
    till.year = year
    till.month = month
    const dateFilter = `${from.year}-${from.month}/${till.year}-${till.month}`
    this.props._handleUpdate(dateFilter)
    this.setState({ till: { till } })
  }

  handleClose = entity => {
    const state = this.state[entity]
    state.open = false
    this.setState({ [entity]: state })
  }


  render = () => (
    <div className="DateFilter">
      с {' '}&nbsp;
      <Button
        color="secondary"
        size="sm"
        onClick={() => this.handleClick('from', 'till')}
      >
        {this.state.from.year}-{this.state.from.month}
      </Button>
      {' '} по&nbsp;
      <Button
        color="secondary"
        size="sm"
        onClick={() => this.handleClick('till', 'from')}
      >
        {this.state.till.year}-{this.state.till.month}
      </Button>

      <div className="FromSelector">
        <YearMonthSelector
          year={this.state.from.year}
          month={this.state.from.month - 1}
          onChange={(year, month) => this.handleFromChange(year, month+1)}
          open={this.state.from.open}
          onClose={() => this.handleClose('from')}
        />
      </div>
      <div className="TillSelector">
        <YearMonthSelector
          year={this.state.till.year}
          month={this.state.till.month - 1}
          onChange={(year, month) => this.handleTillChange(year, month+1)}
          open={this.state.till.open}
          onClose={() => this.handleClose('till')}
        />
      </div>
    </div>
  )
}
