import React, { Component } from 'react'
import fetch from 'node-fetch'
import styled from 'styled-components'
//
import './index.css'
import { XHRPost, latlngsToWKT } from '../../helpers'
import { BACK, THRASHER } from '../../constants'
import DefaultRegionLayer, { DefaultRegionControls } from './Layers/DefaultRegion'
import TrustedFishingPointsControls from './Layers/TrustedFishingPoints/Controls'
import TrustedFishingPointsLayer from './Layers/TrustedFishingPoints/TrustedPointsList'
import ExpectedControls from './Layers/Expected/Controls'
import ExpectedLayer from './Layers/Expected/ExpectedLayer'
import Map from './Map'
import DateFilter from './DateFilter'
import ResearchQuery from './Queries/Research'
import StandartQuery from './Queries/Standart'
//
import ChlorophyllTestLayer from './Layers/ChlorophyllTest'
// import ChlorophyllTestLayer, { ChlorophyllTestControls } from './Layers/ChlorophyllTest'
import HullControls from './Layers/Hull/HullControls'
import Hulls from './Layers/Hull/Hulls'
//
const isBrowser = typeof window !== 'undefined'
const L = isBrowser ? require('leaflet') : undefined
// const RL = isBrowser ? require('react-leaflet') : undefined

const StyledQueries = styled.div`
  .visible {
    // border: dashed 2px orange;
    position: absolute;
    top: 43px;
    right: 0;
    width: 250px;
    background-color: slateblue;
    padding: 0;
    color: white;
  }
  h2 {
    padding: 8px 0 0 28px;
    color: darkslateblue;
    font-size: 20px;
  }
`

export default class Main extends Component {
  state = {
    visible: {
      queries: true,
    },
    // ready: false,
    // data: [],
    defaultRegion: true,
    chlorophyllTest: false,
    hull: true,
    hulls: [],
    trustedFishingPoints: false,
    expectedFishingPoints: false,
    dateFilter: '2015-01/2018-12',

    // Map component props
    regionCenter: L.latLng(68, 6),

    // Filter component props
    date: '2016-09-1',
    characteristics: [],
  }

  componentDidMount = () => {
    // Asynchronous loading characteristics data from backend
    // Get filter data
    /*
    fetch(`${BACK}/ocean/getfilter/`)
      .then(res => res.json())
      .then(data => {
        const thischaracteristics = []
        // Forming characteristics checkbox options:
        data.characteristics.forEach(element => {
          if (element.model !== 'Bathymetric'
              && element.model !== 'SeaWindSpeed'
          ) {
            thischaracteristics.push({
              label: element.title,
              value: element.model,
              checked:
                element.model!=='SeaSurfaceTemperature',
            })
          }
        })
        // Update component state
        this.setState({ characteristics: thischaracteristics })
      })
    */
    // Get hulls boundings data
    fetch(`${THRASHER}/ocean/quickhulltest/`)
      .then(res => res.json())
      .then(json => {
        const thishulls = []
        json.forEach((set, setkey) => {
          if (set.metafiles.length > 0) {
            const thismetafiles = []
            set.metafiles.forEach((file, filekey) => {
              thismetafiles.push({
                id: Number(String(setkey) + String(filekey)),
                filename: file.filename,
                // Get hull points from JSON
                hull: JSON.parse(file.quickHull),
                // Set default hull layer visibility
                visible: false,
              })
            })
            if (thismetafiles.length > 0) {
              thishulls.push({
                id: setkey,
                dirname: set.dirname,
                metafiles: thismetafiles,
                // Set default hull directory visibility
                visible: false,
              })
            }
          }
        })
        // Update component state
        this.setState({ hulls: thishulls })
      })
  }

  // Get positions to render Leaflet polygon
  getRegionPositions = (center, type) => {
    type = (typeof type === 'undefined')?0:type

    const step = 1
    let path = []
    const x = center.lng
    const y = center.lat

    switch (type) {
      case -1:
        path = [[]]
        break
      case 0:
        path = [[
          [y - step/2, x - step],
          [y + step/2, x - step],
          [y + step/2, x + step],
          [y - step/2, x + step],
        ]]
        break
      default:
        path = [[]]
    }

    return path
  }

  handleSubmit = () => {
    // Get characteristics titles as array
    const characteristics = []
    this.state.characteristics.forEach(element => {
      if (element.checked) {
        characteristics.push(element.value)
      }
    })

    const submit = {
      // Get polygon by center and convert it to WKT
      region: latlngsToWKT(
        this.getRegionPositions(this.state.regionCenter)),
      date: this.state.date,
      characteristics: { characteristics },
    }
    // console.log('>> ', submit)

    // Sending POST request to backend with submit array data
    XHRPost(
      `${BACK}/dump/create/`,
      JSON.stringify(submit),
      () => {
        window.location.href = '/queries'
      }
    )
    // Navigate to queries screen
    // this.props.history.push('/queries')
  }

  handleStateKeyValueUpdate = (key, value) => {
    // console.log('state key value update: ' + key + ' ' + value);
    // Update component state by given key and value
    this.setState({ [key]: value })
  }

  setLayer = (key, value) => {
    this.setState({ [key]: value })
  }

  setHullDir = (id, value) => {
    const thishulls = this.state.hulls
    thishulls.forEach((dir, key) => {
      if (dir.id === id) {
        thishulls[key].visible = value
        dir.metafiles.map(file => {
          file.visible = value
          return file
        })
      }
    })
    this.setState({ hulls: thishulls })
  }

  setHull = (id, value) => {
    const thishulls = this.state.hulls
    thishulls.forEach(dir => {
      dir.metafiles.forEach(file => {
        if (file.id === id) {
          file.visible = value
        }
      })
    })
    this.setState({ hulls: thishulls })
  }

  handleFilterUpdate = dateFilter => {
    this.setState({ dateFilter })
  }

  render () {
    // console.log('characteristics: ', this.state.characteristics)
    // console.log('hulls >', this.state.hulls)
    return (
      <div className="main">
        <div className="layers">
          <h2>Слои данных</h2>
          <DefaultRegionControls
            name="defaultRegion"
            _setLayer={this.setLayer}
            checked={this.state.defaultRegion}
          />
          <TrustedFishingPointsControls
            name="trustedFishingPoints"
            _setLayer={this.setLayer}
            checked={this.state.trustedFishingPoints}
          />
          <ExpectedControls
            name="expectedFishingPoints"
            _setLayer={this.setLayer}
            checked={this.state.expectedFishingPoints}
          />

          {/*
          <ChlorophyllTestControls
            name="chlorophyllTest"
            _setLayer={this.setLayer}
            checked={this.state.chlorophyllTest}
          />
          */}

          <HullControls
            className="hull-controls"
            name="hull"
            _setLayer={this.setLayer}
            _setHullDir={this.setHullDir}
            _setHull={this.setHull}
            checked={this.state.hull}
            hulls={this.state.hulls}
          />
        </div>
        <DateFilter _handleUpdate={this.handleFilterUpdate} />
        <Map
          regionPositions={this.getRegionPositions(this.state.regionCenter)}
          handleStateKeyValueUpdate={this.handleStateKeyValueUpdate}
          layer00={this.state.layer00}
          layer01={this.state.layer01}
          layer02={this.state.layer02}
        >
          { /* Default region with processed data */}
          <DefaultRegionLayer visible={this.state.defaultRegion} />

          { /* Trusted fishing points locations */ }
          <TrustedFishingPointsLayer
            visible={this.state.trustedFishingPoints}
            dateFilter={this.state.dateFilter}
          />

          { /* Expected fishing points locations */ }
          <ExpectedLayer
            visible={this.state.expectedFishingPoints}
            dateFilter={this.state.dateFilter}
          />

          { /* Теst data from .nc file */}
          <ChlorophyllTestLayer visible={this.state.chlorophyllTest} />
          <Hulls hulls={this.state.hulls} visible={this.state.hull} />
          { /* <HullLayer visible={this.state.hull} hulls={this.state.hulls} /> */ }
        </Map>
        <StyledQueries>
          {this.state.visible.queries ?
            <div className="visible">
              <h2>Запросы</h2>
              <StandartQuery
                characteristics={this.state.characteristics}
                handleSubmit={this.handleSubmit}
                handleStateKeyValueUpdate={this.handleStateKeyValueUpdate}
                submitStatus="ready"
              />
              <ResearchQuery />
            </div>
            :
            <div className="hidden">
              X
            </div>
          }
        </StyledQueries>
        {/*
        */}
      </div>
    )
  }
}
