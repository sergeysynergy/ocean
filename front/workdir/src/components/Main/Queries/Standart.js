import React from 'react'
import PropTypes from 'prop-types'
import { Button } from 'reactstrap'

import './index.css'
import { ZSelect, ZCheckbox } from '../../includes/ZBootstrap'
import CollapseBar from '../../includes/HandyBar/CollapseBar'


const HEIGHT = window.innerHeight - 43

export default class ProcessedData extends React.Component {
  static propTypes = {
    handleStateKeyValueUpdate: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
  }
  constructor (props) {
    super(props)
    this.state = {
      width: 200,
      height: HEIGHT,
      // ready: false,
      collapsed: false,
    }
    this.style = {
      height: this.state.height,
      width: this.state.width,
    }
    this.handleResize = this.handleResize.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)

    // Forming date select list options
    this.datesData = []
    for (let i=1; i<31; i++) {
      this.datesData.push({
        value: `2016-09-${i}`,
        label: `2016-09-${i}`,
      })
    }
  }
  componentDidMount () {
    window.addEventListener('resize', this.handleResize)
  }

  handleResize () {
    this.setState({ height: HEIGHT })
  }
  handleSubmit () {
    console.log('filter submit')
    // this.props.handleSubmit(e)
    // this.setState({submit: this.init})
  }

  handleCollapse = collapsed => {
    this.setState({ collapsed: { collapsed } })
  }

  render () {
    // return <div className='Filter' style={this.style}>
    return (
      <div className="Query">
        <CollapseBar
          useCheck={false}
          collapsed={this.state.collapsed}
          _handleCollapse={this.handleCollapse}
          lable="Обработанные характеристики"
        />
        {!this.state.collapsed &&
        <form>
          <ZSelect
            name="date"
            // title='Дата'
            defaultValue={this.props.date}
            options={this.datesData}
            handleStateKeyValueUpdate={this.props.handleStateKeyValueUpdate}
          />
          <ZCheckbox
            name="characteristics"
            title="Характеристики"
            options={this.props.characteristics}
            handleStateKeyValueUpdate={this.props.handleStateKeyValueUpdate}
          />
          <SubmitButton
            status={this.props.submitStatus}
            handleSubmit={this.props.handleSubmit}
          />
        </form>
        }
      </div>
    )
  }
}
/*
          <ZText
            name='characteristics'
            lable='Пометка'
            help='Пометка облегчит поиск дампа файла'
            _handleChange={this.handleChange}
          />
        <PointsNumber
          value={this.state.filter.points}
          _handleChange={this.handleChange}
        />
      */


class SubmitButton extends React.Component {
  static propTypes = {
    status: PropTypes.string,
  }
  static defaultProps = {
    status: 'init',
  }
  constructor (props) {
    super(props)
    this.state = {
      status: this.props.status,
    }
    this.className = 'SubmitButton'
    this.values = {
      init: 'Получаю данные...',
      ready: 'Отправить запрос',
      progress: 'Запрос отправлен',
    }
    this.handleClick = this.handleClick.bind(this)
  }
  handleClick (e) {
    // console.log('clicked');
    if (this.state.status === 'ready') {
      this.props.handleSubmit(e)
      // this.setState({ status: 'progress' })
    }
  }

  render () {
    // console.log('status >', this.state.status);
    return (
      <div className={this.className}>
        <Button
          // bsSize="small"
          block
          disabled={this.state.status==='ready'}
          onClick={this.handleClick}
          color={this.state.status==='progress'?'success':'primary'}
        >
          {this.values[this.state.status]}
        </Button>
      </div>
    )
  }
}
