import React from 'react'
import { Route, Switch } from 'react-router-dom'
// import { injectGlobal } from 'styled-components'
//
import MainMenu from './includes/MainMenu'
import Main from './Main/'
import UserQueries from './UserQueries'
import About from './About'
import Tests from './Tests'
import DataPoints from './DataPoints'

/*
injectGlobal`
  body {
    background-color: #eee;
    color: #222;
    margin: 0;
  }
`
*/


export default () => (
  <div>
    <MainMenu />
    <Switch>
      <Route exact path="/" component={Main} />
      <Route exact path="/about" component={About} />
      <Route exact path="/queries" component={UserQueries} />
      <Route exact path="/tests" component={Tests} />
      <Route exact path="/tests/datapoints" component={DataPoints} />
    </Switch>
  </div>
)

// <Route path="/queries" component={Queries}/>
