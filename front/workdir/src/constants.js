export const GC_USER_ID = 'graphene-user-id'
export const GC_AUTH_TOKEN = 'graphene-auth-token'
export const GC_USER_NAME = 'graphene-user-name'

export const THRASHER = `http://${window.location.hostname}:21025`

export const FRONT = `http://${window.location.hostname}:21010`

// export const BACK = 'http://ocean.cttgroup.ru:21018'
export const BACK = `http://${window.location.hostname}:21018`
// export const BACK = 'http://127.0.0.1:21018'

export const ITEMS_PER_PAGE = 10
