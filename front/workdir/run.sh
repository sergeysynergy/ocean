#!/bin/bash

if [[ $1 = "dev" ]]; then
  yarn start

  exit 1
fi

if [[ $1 = "start" ]]; then
  yarn start

  exit 1
fi

if [[ $1 = "relay" ]]; then
  clear
  yarn run relay-compiler --src ./src --schema /data/schema.json

  exit 1
fi
