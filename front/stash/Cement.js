exports.SMALL = 768
exports.MEDIUM = 992
exports.PORT = 28002

exports.XHRGet = (url, callback) => {
  // console.log("URL", url);
  const xhr = new XMLHttpRequest()
  xhr.responseType = 'json'
  xhr.open('GET', url, true)
  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4) {
      // Request finished. Do processing here.
      if (xhr.status === 200 || xhr.status === 204) {
        // Got positive response
        // console.log("DATA",xhr.response)
        if (callback && typeof callback === 'function') {
          callback(xhr.response)
        }
      } else {
        // Got error
        console.log(`Error: ${xhr.status}`)
      }
    }
  }
  xhr.send()
}


exports.XHRPost = (url, attributes, callback) => {
  const xhr = new XMLHttpRequest()
  xhr.responseType = 'json'
  xhr.open('POST', url, true)
  xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')
  xhr.onreadystatechange = () => {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      // Request finished. Do processing here.
      if (xhr.status === 200 || xhr.status === 204) {
        // Got positive response
        // console.log("DATA",xhr.response)
        if (callback && typeof callback === 'function') {
          callback(xhr.response)
        }
      } else {
        // Got error
        console.log(`Error: ${xhr.status}`)
      }
    }
  }
  xhr.send(attributes)
}


exports.latlngsToWKT = latlngs => {
  let wkt = 'SRID=4326;MULTIPOLYGON('
  latlngs.forEach(polygon => {
    wkt +='('
    polygon.forEach(point => {
      wkt += `${point[0]} ${point[1]}, `
    })
    wkt +=')'
  })
  wkt +=')'
  return wkt
}
