import React, { Component } from 'react'
import PropTypes from 'prop-types'
//
import CheckBar from '../../../includes/HandyBar/CheckBar'
import './style.css'


export default class HullControls extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    _setLayer: PropTypes.func.isRequired,
    _setHull: PropTypes.func.isRequired,
    checked: PropTypes.bool,
    hulls: PropTypes.array,
  }
  static defaultProps = {
    checked: true,
    hulls: [],
  }

  handleCheck = checked => {
    this.props._setLayer(this.props.name, checked)
  }

  handleHullCheck = (checked, id) => {
    // this.props._setLayer(this.props.name, checked)
    // console.log('checked >', checked, id)
    this.props._setHull(id, checked)
  }

  render = () => {
    const hulls = this.props.hulls.map(hull => (
      <CheckBar
        key={hull.id}
        id={hull.id}
        checked={hull.visible}
        _handleCheck={this.handleHullCheck}
        lable={hull.filename}
      />
    ))

    return (
      <div className="Hull">
        <h2>
          <CheckBar
            checked={this.props.checked}
            _handleCheck={this.handleCheck}
            lable="Границы данных"
          />
        </h2>
        {this.props.checked &&
          <div>
            {hulls}
          </div>
        }
      </div>
    )
  }
}
