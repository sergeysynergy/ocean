import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'

import environment from '../../../Environment'
import ExpectedfishingpointsListItem from './ExpectedfishingpointsListItem'
/* End of import section ***************************************/


const ExpectedfishingpointsListQuery = graphql`
  query ExpectedfishingpointsListQuery ($count: Int!, $after: String!) {
    allExpectedfishingpoints(
      first: $count
      after: $after
    ) @connection(
      key: "Expectedfishingpoints_allExpectedfishingpoints",
      filters: []
    ) {
      edges {
        node {
          ...ExpectedfishingpointsListItem_point
        }
      }
      pageInfo {
        hasNextPage
        endCursor
      }
    }
  }
`
/* End of constants and variables section ****************************/


class Points extends Component {
  state = {
    after: "",
  }

  _loadMore(after) {
    this.setState({after: after})
  }

  render() {
    return <QueryRenderer
      environment={environment}
      query={ExpectedfishingpointsListQuery}
      variables={{
        count: 100,
        after: this.state.after,
      }}
      render={({error, props}) => {
        // console.log('props', this.props);
        if (error) {
          return <div>{error.message}</div>
        } else if (props) {
          // console.log('hm', props);
          let after = props.allExpectedfishingpoints.pageInfo.endCursor
          let next = props.allExpectedfishingpoints.pageInfo.hasNextPage
          if (next) {
            this._loadMore(after)
          }

          return <div>
            {props.allExpectedfishingpoints.edges.map(({node}) => {
              return <ExpectedfishingpointsListItem
                key={node.__id}
                point={node}
              />
            })}
          </div>
        }
        return <div>Loading</div>
      }}
    />
  }
}


export default Points
