import React, { Component } from 'react'
import { createFragmentContainer, graphql } from 'react-relay'


const isBrowser = typeof window !== 'undefined';
const RL = isBrowser ? require('react-leaflet') : undefined

/* End of import section ***************************************/

class ExpectedfishingpointsItem extends Component {
  render() {
    return <RL.Circle
      key={this.props.point.id}
      center={[this.props.point.lat, this.props.point.lon]}
      radius={10}
      color={'#ffcc00'}
    />
  }
}

export default createFragmentContainer(ExpectedfishingpointsItem, graphql`
  fragment ExpectedfishingpointsListItem_point on ExpectedfishingpointNode {
    id
    lat
    lon
    datetime
  }
`)
