import React, { Component } from 'react'
import { Button } from 'reactstrap'
import YearMonthSelector from 'react-year-month-selector'
import './YearMonthSelector.css'


class FishingPointsFilter extends Component {
  state = {
    open: true,
    from: {
      year: this.props.year,
      month: this.props.month - 1,
      open: false,
    },
    till: {
      year: this.props.year,
      month: this.props.month - 1,
      open: false,
    },
  }

  handleClick = (entity1, entity2) => {
    let state1 = this.state[entity1]
    state1.open = !state1.open

    let state2 = this.state[entity2]
    state2.open = false

    this.setState({[entity1]: state1, [entity2]: state2})
  }


  handleChange = (entity, year, month) => {
    let state = this.state[entity]
    state.year = year
    state.month = month
    this.setState({[entity]: state})
  }

  handleClose = (entity) => {
    let state = this.state[entity]
    state.open = false
    this.setState({[entity]: state})
  }

  render = () => {
    return <div>
      <div className='FishingPointsFilter'>
        Точки рыбных скоплений: с {' '}
        <Button
          color="primary"
          onClick={() => this.handleClick('from', 'till')}
        >
          {this.state.from.year}-{this.state.from.month + 1}
        </Button>
        {' '} по {' '}
        <Button
          color="primary"
          onClick={() => this.handleClick('till', 'from')}
        >
          {this.state.till.year}-{this.state.till.month + 1}
        </Button>
      </div>
      <div className='FromSelector'>
        <YearMonthSelector
          year={this.state.from.year}
          month={this.state.from.month}
          onChange={(year, month)=> this.handleChange('from', year, month)}
          open={this.state.from.open}
          onClose={() => this.handleClose('from')}
        />
      </div>
      <div className='TillSelector'>
        <YearMonthSelector
          year={this.state.till.year}
          month={this.state.till.month}
          onChange={(year, month)=> this.handleChange('till', year, month)}
          open={this.state.till.open}
          onClose={() => this.handleClose('till')}
        />
      </div>
    </div>
  }
}


export default FishingPointsFilter
