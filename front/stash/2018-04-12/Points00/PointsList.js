import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'

import environment from '../../Environment'
import PointsListItem from './PointsListItem'
/* End of import section ***************************************/


const PointsListQuery = graphql`
  query PointsListQuery {
    allExactFishingPoints(last: 100) @connection(key: "Points_allExactFishingPoints", filters: []) {
      edges {
        node {
          ...PointsListItem_point
        }
      }
    }
  }
`
/* End of constants and variables section ****************************/


class Points extends Component {
  render() {
    return <QueryRenderer
      environment={environment}
      query={PointsListQuery}
      render={({error, props}) => {
        if (error) {
          return <div>{error.message}</div>
        } else if (props) {
          // console.log('props', props);
          return <div>
            {props.allExactFishingPoints.edges.map(({node}) => {
              return <PointsListItem
                key={node.__id}
                point={node}
              />
            })}
          </div>
        }
        return <div>Loading</div>
      }}
    />
  }
}


export default Points
