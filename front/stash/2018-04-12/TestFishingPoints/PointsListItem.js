import React, { Component } from 'react'
import { createFragmentContainer, graphql } from 'react-relay'


/* End of import section ***************************************/

class Point extends Component {
  render() {
    return <div key={this.props.point.id}>
      {this.props.point.id}|||
      {this.props.point.lat}|
      {this.props.point.lon}|
      {this.props.point.datetime}
    </div>
  }
}

export default createFragmentContainer(Point, graphql`
  fragment PointsListItem_point on ExactFishingPointNode {
    id
    lat
    lon
    datetime
  }
`)
