export function arraySimpleSortByOrder(arr) {
  let a = arr.map(x => x)
  let i = a.length - 1
  for (i; i>=0; i--) {
    for (let j=0; j<i; j++) {
      if (a[j].order > a[j+1].order) {
        let tmp = a[j]
        a[j] = a[j+1]
        a[j+1] = tmp
      }
    }
  }
  let normalized = a.map((item, key) => {
    let copy = Object.assign({}, item)
    copy.order = key
    return copy
  })
  return normalized
}

export function arrayMoveByOrder(arr, oldIndex, newIndex) {
  let a = arr.map(x => x)
  let tmp = arr[oldIndex]
  a.splice(oldIndex, 1)
  a.splice(newIndex, 0, tmp)
  return a
}

/*
Array.prototype.swap = function (x,y) {
  var b = this[x];
  this[x] = this[y];
  this[y] = b;
  return this;
}

export function arrayQuickSortByOrder(a, first, last) {
  let i = first
  let j = last
  let x = a.order[(first + last) / 2]
  do {
    while (a[i] < x) i++
    while (a[j] > x) j--
    if(i <= j) {
      if (i < j) a.swap(i, j)
      i++
      j--
    }
  } while (i <= j)
  if (i < last)
    arrayQuickSortByOrder(a, i, last)
  if (first < j)
    arrayQuickSortByOrder(a, i, last)
}
*/
