/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteFragment } from 'relay-runtime';
import type { FragmentReference } from 'relay-runtime';
declare export opaque type PointsListItem_point$ref: FragmentReference;
export type PointsListItem_point = {|
  +id: string,
  +lat: number,
  +lon: number,
  +datetime: any,
  +$refType: PointsListItem_point$ref,
|};
*/


const node/*: ConcreteFragment*/ = {
  "kind": "Fragment",
  "name": "PointsListItem_point",
  "type": "ExactFishingPointNode",
  "metadata": null,
  "argumentDefinitions": [],
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "id",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "lat",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "lon",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "datetime",
      "args": null,
      "storageKey": null
    }
  ]
};
(node/*: any*/).hash = '7e071d5167c50a31c607de2577d7d445';
module.exports = node;
