/**
 * @flow
 * @relayHash f7730991c23c842dbc7b840aea8180e8
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type PointsListItem_point$ref = any;
export type PointsListQueryVariables = {|
  after: string,
|};
export type PointsListQueryResponse = {|
  +allExactFishingPoints: ?{|
    +edges: $ReadOnlyArray<?{|
      +node: ?{|
        +$fragmentRefs: PointsListItem_point$ref,
      |},
    |}>,
    +pageInfo: {|
      +hasNextPage: boolean,
      +endCursor: ?string,
    |},
  |},
|};
*/


/*
query PointsListQuery(
  $after: String!
) {
  allExactFishingPoints(first: 30, after: $after) {
    edges {
      node {
        ...PointsListItem_point
        id
        __typename
      }
      cursor
    }
    pageInfo {
      hasNextPage
      endCursor
    }
  }
}

fragment PointsListItem_point on ExactFishingPointNode {
  id
  lat
  lon
  datetime
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "after",
    "type": "String!",
    "defaultValue": null
  }
],
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "__typename",
  "args": null,
  "storageKey": null
},
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "cursor",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "LinkedField",
  "alias": null,
  "name": "pageInfo",
  "storageKey": null,
  "args": null,
  "concreteType": "PageInfo",
  "plural": false,
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "hasNextPage",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "endCursor",
      "args": null,
      "storageKey": null
    }
  ]
};
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "PointsListQuery",
  "id": null,
  "text": "query PointsListQuery(\n  $after: String!\n) {\n  allExactFishingPoints(first: 30, after: $after) {\n    edges {\n      node {\n        ...PointsListItem_point\n        id\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      hasNextPage\n      endCursor\n    }\n  }\n}\n\nfragment PointsListItem_point on ExactFishingPointNode {\n  id\n  lat\n  lon\n  datetime\n}\n",
  "metadata": {
    "connection": [
      {
        "count": null,
        "cursor": "after",
        "direction": "forward",
        "path": [
          "allExactFishingPoints"
        ]
      }
    ]
  },
  "fragment": {
    "kind": "Fragment",
    "name": "PointsListQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": "allExactFishingPoints",
        "name": "__Points_allExactFishingPoints_connection",
        "storageKey": null,
        "args": null,
        "concreteType": "ExactFishingPointNodeConnection",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "edges",
            "storageKey": null,
            "args": null,
            "concreteType": "ExactFishingPointNodeEdge",
            "plural": true,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "node",
                "storageKey": null,
                "args": null,
                "concreteType": "ExactFishingPointNode",
                "plural": false,
                "selections": [
                  {
                    "kind": "FragmentSpread",
                    "name": "PointsListItem_point",
                    "args": null
                  },
                  v1
                ]
              },
              v2
            ]
          },
          v3
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "PointsListQuery",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "allExactFishingPoints",
        "storageKey": null,
        "args": [
          {
            "kind": "Variable",
            "name": "after",
            "variableName": "after",
            "type": "String"
          },
          {
            "kind": "Literal",
            "name": "first",
            "value": 30,
            "type": "Int"
          }
        ],
        "concreteType": "ExactFishingPointNodeConnection",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "edges",
            "storageKey": null,
            "args": null,
            "concreteType": "ExactFishingPointNodeEdge",
            "plural": true,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "node",
                "storageKey": null,
                "args": null,
                "concreteType": "ExactFishingPointNode",
                "plural": false,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "id",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "lat",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "lon",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "datetime",
                    "args": null,
                    "storageKey": null
                  },
                  v1
                ]
              },
              v2
            ]
          },
          v3
        ]
      },
      {
        "kind": "LinkedHandle",
        "alias": null,
        "name": "allExactFishingPoints",
        "args": [
          {
            "kind": "Variable",
            "name": "after",
            "variableName": "after",
            "type": "String"
          },
          {
            "kind": "Literal",
            "name": "first",
            "value": 30,
            "type": "Int"
          }
        ],
        "handle": "connection",
        "key": "Points_allExactFishingPoints",
        "filters": []
      }
    ]
  }
};
})();
(node/*: any*/).hash = 'bbae7ca46feea0528e818601d699715d';
module.exports = node;
