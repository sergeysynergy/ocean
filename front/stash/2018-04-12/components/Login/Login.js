import React, { Component } from 'react'
import {
  Jumbotron, Input, FormGroup, Button, Label,
} from 'reactstrap'

import './Login.css'
import { USER_ID, AUTH_TOKEN, USER_NAME } from '../../settings'
import SigninUserMutation from '../../mutations/SigninUserMutation'
import CreateUserMutation from '../../mutations/CreateUserMutation'


class Login extends Component {
  state = {
    login: true, // switch between Login and SignUp
    email: '',
    password: '',
    username: ''
  }

  render() {
    return (
      <div className='Content Login'>
        <Jumbotron>
          <h2>{this.state.login ? 'Вход' : 'Регистрация'}</h2>
          {!this.state.login &&
          <FormGroup>
            <Label>Имэил</Label>
            <Input
              type="text"
              value={this.state.email}
              placeholder="Адрес электронной почты"
              onChange={(e) => this.setState({ email: e.target.value })}
            />
          </FormGroup>}
          <FormGroup>
            <Label>Имя</Label>
            <Input
              type="text"
              value={this.state.username}
              placeholder="Имя пользователя"
              onChange={(e) => this.setState({ username: e.target.value })}
            />
          </FormGroup>
          <FormGroup>
            <Label>Пароль</Label>
            <Input
              type="password"
              value={this.state.password}
              placeholder='Пароль'
              onChange={(e) => this.setState({ password: e.target.value })}
            />
          </FormGroup>
          <Button
            color="primary"
            onClick={() => this._confirm()}
          >
            {this.state.login ? 'Вход' : 'Регистрация' }
          </Button>
          <a
            className='Link'
            onClick={() => this.setState({ login: !this.state.login })}
          >
            {this.state.login ? 'Необходимо зарегистрироваться?' : 'Уже есть аккаунт?'}
          </a>
        </Jumbotron>
      </div>
    )
  }

  _confirm = () => {
    const { username, email, password } = this.state
    if (this.state.login) {
      SigninUserMutation(username, password, (id, token, username) => {
        this._saveUserData(id, token, username)
        this.props.history.push(`/`)
      })
    } else {
      CreateUserMutation(username, email, password, (id, token, username) => {
        this._saveUserData(id, token, username)
        this.props.history.push(`/`)
      })
    }
  }

  _saveUserData = (id, token, username) => {
    localStorage.setItem(USER_ID, id)
    localStorage.setItem(AUTH_TOKEN, token)
    localStorage.setItem(USER_NAME, username)
  }
}

export default Login
