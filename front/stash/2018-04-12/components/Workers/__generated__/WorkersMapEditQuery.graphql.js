/**
 * @flow
 * @relayHash 67f86e63162849a12ced5b2d6abd085d
 */

/* eslint-disable */

'use strict';

/*::
import type {ConcreteBatch} from 'relay-runtime';
export type WorkersMapEditQueryResponse = {|
  +worker: ?{| |};
|};
*/


/*
query WorkersMapEditQuery(
  $workerId: ID!
) {
  worker(id: $workerId) {
    ...WorkersEdit_worker
    id
  }
}

fragment WorkersEdit_worker on WorkerNode {
  id
  name
  position
  experience
  description
  link
}
*/

const batch /*: ConcreteBatch*/ = {
  "fragment": {
    "argumentDefinitions": [
      {
        "kind": "LocalArgument",
        "name": "workerId",
        "type": "ID!",
        "defaultValue": null
      }
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "WorkersMapEditQuery",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": [
          {
            "kind": "Variable",
            "name": "id",
            "variableName": "workerId",
            "type": "ID!"
          }
        ],
        "concreteType": "WorkerNode",
        "name": "worker",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "WorkersEdit_worker",
            "args": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query"
  },
  "id": null,
  "kind": "Batch",
  "metadata": {},
  "name": "WorkersMapEditQuery",
  "query": {
    "argumentDefinitions": [
      {
        "kind": "LocalArgument",
        "name": "workerId",
        "type": "ID!",
        "defaultValue": null
      }
    ],
    "kind": "Root",
    "name": "WorkersMapEditQuery",
    "operation": "query",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": [
          {
            "kind": "Variable",
            "name": "id",
            "variableName": "workerId",
            "type": "ID!"
          }
        ],
        "concreteType": "WorkerNode",
        "name": "worker",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "args": null,
            "name": "id",
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "args": null,
            "name": "name",
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "args": null,
            "name": "position",
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "args": null,
            "name": "experience",
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "args": null,
            "name": "description",
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "args": null,
            "name": "link",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "text": "query WorkersMapEditQuery(\n  $workerId: ID!\n) {\n  worker(id: $workerId) {\n    ...WorkersEdit_worker\n    id\n  }\n}\n\nfragment WorkersEdit_worker on WorkerNode {\n  id\n  name\n  position\n  experience\n  description\n  link\n}\n"
};

module.exports = batch;
