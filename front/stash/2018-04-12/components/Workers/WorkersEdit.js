import React, { Component } from 'react'
import { createFragmentContainer, graphql } from 'react-relay'
import { Button } from 'reactstrap'

import './Workers.css'
import { Text, Textarea } from '../forms'
import UpdateWorkerMutation from '../../mutations/UpdateWorkerMutation'
/* End of import section ***/


class WorkersEdit extends Component {
  state = {
    id: this.props.worker.id,
    name: this.props.worker.name,
    position: this.props.worker.position,
    experience: this.props.worker.experience,
    description: this.props.worker.description,
    link: this.props.worker.link,
    order: this.props.worker.order,
  }

  render() {

    return <div>
      <Text
        label='Фамилия Имя Отчество'
        attribute='name'
        self={this}
      />
      <Text
        label='Должность'
        attribute='position'
        self={this}
      />
      <Text
        label='Опыт'
        attribute='experience'
        self={this}
      />
      <Textarea
        label='Описание'
        attribute='description'
        self={this}
      />
      <Text
        label='Ссылка'
        attribute='link'
        self={this}
      />
      <Button
        color="primary"
        onClick={() => this._saveWorker()}
      >
        Сохранить
      </Button>
    </div>
  }

  _saveWorker = () => {
    const {
      id,
      name,
      position,
      experience,
      description,
      link,
      order,
    } = this.state
    UpdateWorkerMutation(
      id,
      name,
      position,
      experience,
      description,
      link,
      order,
      () => this.props.history.push(this.props.base)
    )
  }
}

export default createFragmentContainer(WorkersEdit, graphql`
  fragment WorkersEdit_worker on WorkerNode {
    id
    name
    position
    experience
    description
    link
  }
`)
