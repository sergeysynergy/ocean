import React, { Component } from 'react'
import { createFragmentContainer, graphql } from 'react-relay'
import { Link } from 'react-router-dom'

/* End of import section ***************************************/

class WorkersListItem extends Component {
  render() {

    return <div>
      <Link to={this.props.base + '/' + this.props.worker.id}>
        {this.props.worker.name}
      </Link>
    </div>
  }
}

export default createFragmentContainer(WorkersListItem, graphql`
  fragment WorkersListItem_worker on WorkerNode {
    id
    name
    order
  }
`)
