import React, { Component } from 'react'
import { Button } from 'reactstrap'

import './Menu.css'

/* End of import section */

class Delete extends Component {
  render() {
    const worker = this.props.data

    return (
      <div className='WorkersDelete'>
        <h1>Удаление сотрудника</h1>
        <div>ФИО: <span className='Name'>{worker.name}</span></div>
        <div>ID: <span className='Name'>{worker.id}</span></div>
        <Button
          color="danger"
          onClick={() => {
            this.props._deleteWorker(worker.id)
            this.props.history.push(this.props.url.base)
          }}
        >
          Да, удалить
        </Button>
      </div>
    )
  }
}


export default Delete
