import React, { Component } from 'react'
import {
  Input, FormGroup, Button, Label
} from 'reactstrap'

/* End of import section */

class CreateItem extends Component {
  state = {
    title: '',
    slug: '',
    parent: null,
  }

  buildItems = (items, level=0) => {
    return items.map((item, key) => {
      if (item.children) {
        this.buildItems(item.children, level++)
      }
      return <option key={key} value={item.id}>
        {'-'.repeat((level + 1) * 2)}
        {' ' + item.title}
        </option>
    })
  }

  render() {
    let select =
      <FormGroup>
        <Label for="exampleSelect">Родительский раздел</Label>
        <Input
          type="select"
          name="select"
          id="exampleSelect"
          onChange={(e) => this.setState({ parent: e.target.value })}
        >
          <option>Корень</option>
          {this.buildItems(this.props.data)}
        </Input>
      </FormGroup>

    return (
      <div className='WorkersEdit'>
        <h1>Добавление пункта меню</h1>
        {select}
        <FormGroup>
          <Label>Название</Label>
          <Input
            type="text"
            value={this.state.title}
            onChange={(e) => this.setState({ title: e.target.value })}
          />
        </FormGroup>
        <FormGroup>
          <Label>Адрес</Label>
          <Input
            type="text"
            value={this.state.slug}
            onChange={(e) => this.setState({ slug: e.target.value })}
          />
        </FormGroup>

        <Button
          color="primary"
          onClick={() => {
            this.props._createItem(this.state)
            this.props.history.push(this.props.url.base)
          }}
        >
          Добавить
        </Button>
      </div>
    )
  }
}


export default CreateItem
