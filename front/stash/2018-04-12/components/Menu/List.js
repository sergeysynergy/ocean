import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import './Menu.css'

/* End of import section */
/* End of constants and variables section */

class List extends Component {
  prefix = ''
  // menu = []

  buildItems = (items, level=0) => {
    return items.map((item, key) => {
      let children = ''
      let prefix = '-'.repeat(level * 2)
      let title = ''
      // console.log(level,':',this.prefix,':',item.title,':',item.parent);
      if (item.children && item.children.length > 0) {
        children = this.buildItems(item.children, level+1)
      }

      if (!item.parent || level > 0) {
        title = item.title
      }

      /*
          {level},
          {item.parent}
          |
          {prefix}
          {item.title}
          */

      return <div key={key}>
        {prefix}
        {title}
        <div>
          {children}
        </div>
        </div>
      /*
          {item.children && item.children.length > 0?
            this.buildItems(item.children, level+1)
            :
            ''
          }
      return <div key={key} >
          {this.prefix}
          <Link to={this.props.url.base + '/' + item.id}>{item.title}</Link>
        </div>
        */

      // item.push({item.title})
      // item.push(</div>)
    })
  }
        // {'-'.repeat((level + 1) * 2)}

  render() {
    let items =
      <div>
        {this.buildItems(this.props.data)}
      </div>

    return (
      <div className='ItemsList'>
        {items}
        <hr />
        <hr />
        {this.props.data.map(item => (
          <Item key={item.id} item={item}/>
        ))}
      </div>
    )
  }
}

const Item = ({item}) => (
  <div><Link to={'/menu/' + item.id}>{item.title}</Link></div>
)


export default List
