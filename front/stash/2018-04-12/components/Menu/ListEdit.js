import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import {SortableContainer, SortableElement, SortableHandle} from 'react-sortable-hoc'


import { arrayMoveByOrder } from '../../cement'
import './Menu.css'

/* End of import section */

class ListEdit extends Component {
  state = {
    data: this.props.data,
  }

  onSortEnd = (oldIndex, newIndex) => {
    let newData = arrayMoveByOrder(this.state.data, oldIndex, newIndex)
    this.props._saveData(newData)
    this.setState({
      data: newData,
    })
  }

  render() {
    let items = this.state.data.map(worker => worker.name)
    return (
      <div className='WorkersList'>
        <h1>Редактирование списка</h1>
        <SortableComponent
          items={items}
          _onSortEnd={this.onSortEnd}
        />
      </div>
    )
  }
}

class SortableComponent extends Component {
  render() {
    return <SortableList
      lockAxis={'y'}
      items={this.props.items}
      onSortEnd={({oldIndex, newIndex}) =>
        this.props._onSortEnd(oldIndex, newIndex)
      }
      distance={5}
    />
  }
}

const SortableItem = SortableElement(({value}) => {
  return (
    <li>
      {value}
    </li>
  )
})

const SortableList = SortableContainer(({items}) => {
  return (
    <ul>
      {items.map((value, index) => (
        <SortableItem
          key={`item-${index}`}
          index={index}
          value={value}
        />
      ))}
    </ul>
  )
})

export default ListEdit
