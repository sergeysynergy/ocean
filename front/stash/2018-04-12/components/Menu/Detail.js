import React, { Component } from 'react'

/* End of import section */

class Detail extends Component {
  render() {
    let worker = this.props.data

    return (
      <div className='WorkersDetail'>
          <h1>{worker.name}</h1>
          <div>{worker.order}</div>
          <div>{worker.description}</div>
      </div>
    )
  }
}

        /*
          Slug: {this.props.match.params.slug}
          Query: {this.props.WorkerQuery}

        <h2>{this.props.worker.name}</h2>
        <div>{this.props.worker.experience}</div>
        <div>{this.props.worker.position}</div>
        <div>{this.props.worker.description}</div>
        */


export default Detail
