import React from 'react'
import {
  Input, FormGroup, Label
} from 'reactstrap'


export const Text = ({label, attribute, self}) =>
   <FormGroup>
    <Label>{label}</Label>
    <Input
      type="text"
      value={self.state[attribute]}
      onChange={(e) => self.setState({ [attribute]: e.target.value })}
    />
  </FormGroup>


export const Textarea = ({label, attribute, self}) =>
  <FormGroup>
    <Label>{label}</Label>
    <Input
      type="textarea"
      value={self.state[attribute]}
      onChange={(e) => self.setState({ [attribute]: e.target.value })}
    />
  </FormGroup>
