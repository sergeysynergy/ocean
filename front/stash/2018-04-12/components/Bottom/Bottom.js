import React from 'react'
import { Container, Row, Col, } from 'reactstrap'

import './Bottom.css';


const Bottom = ()=> (<div className='Bottom'>
  <Container>
    <Row className="show-grid">
      <Col xs={12} sm={6} className='One'>
        <div className='P'>
          Frontend: Relay
        </div>
      </Col>
      <Col xs={12} sm={6} className='Two'>
        <div className='P'>
          Backend: Graphene
        </div>
      </Col>
    </Row>
  </Container>
</div>)


export default Bottom
