/**
 * @flow
 * @relayHash a794318587982f1aebf9e877cf6dc9d2
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type Point_point$ref = any;
export type PointListQueryVariables = {| |};
export type PointListQueryResponse = {|
  +allExactFishingPoints: ?{|
    +edges: $ReadOnlyArray<?{|
      +node: ?{|
        +$fragmentRefs: Point_point$ref,
      |},
    |}>,
  |},
|};
*/


/*
query PointListQuery {
  allExactFishingPoints(last: 10) {
    edges {
      node {
        ...Point_point
        id
        __typename
      }
      cursor
    }
    pageInfo {
      hasPreviousPage
      startCursor
    }
  }
}

fragment Point_point on ExactFishingPointNode {
  id
  lat
  lon
  datetime
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "__typename",
  "args": null,
  "storageKey": null
},
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "cursor",
  "args": null,
  "storageKey": null
},
v2 = {
  "kind": "LinkedField",
  "alias": null,
  "name": "pageInfo",
  "storageKey": null,
  "args": null,
  "concreteType": "PageInfo",
  "plural": false,
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "hasPreviousPage",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "startCursor",
      "args": null,
      "storageKey": null
    }
  ]
};
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "PointListQuery",
  "id": null,
  "text": "query PointListQuery {\n  allExactFishingPoints(last: 10) {\n    edges {\n      node {\n        ...Point_point\n        id\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      hasPreviousPage\n      startCursor\n    }\n  }\n}\n\nfragment Point_point on ExactFishingPointNode {\n  id\n  lat\n  lon\n  datetime\n}\n",
  "metadata": {
    "connection": [
      {
        "count": null,
        "cursor": null,
        "direction": "backward",
        "path": [
          "allExactFishingPoints"
        ]
      }
    ]
  },
  "fragment": {
    "kind": "Fragment",
    "name": "PointListQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": [],
    "selections": [
      {
        "kind": "LinkedField",
        "alias": "allExactFishingPoints",
        "name": "__Points_allExactFishingPoints_connection",
        "storageKey": null,
        "args": null,
        "concreteType": "ExactFishingPointNodeConnection",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "edges",
            "storageKey": null,
            "args": null,
            "concreteType": "ExactFishingPointNodeEdge",
            "plural": true,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "node",
                "storageKey": null,
                "args": null,
                "concreteType": "ExactFishingPointNode",
                "plural": false,
                "selections": [
                  {
                    "kind": "FragmentSpread",
                    "name": "Point_point",
                    "args": null
                  },
                  v0
                ]
              },
              v1
            ]
          },
          v2
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "PointListQuery",
    "argumentDefinitions": [],
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "allExactFishingPoints",
        "storageKey": "allExactFishingPoints(last:10)",
        "args": [
          {
            "kind": "Literal",
            "name": "last",
            "value": 10,
            "type": "Int"
          }
        ],
        "concreteType": "ExactFishingPointNodeConnection",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "edges",
            "storageKey": null,
            "args": null,
            "concreteType": "ExactFishingPointNodeEdge",
            "plural": true,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "node",
                "storageKey": null,
                "args": null,
                "concreteType": "ExactFishingPointNode",
                "plural": false,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "id",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "lat",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "lon",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "datetime",
                    "args": null,
                    "storageKey": null
                  },
                  v0
                ]
              },
              v1
            ]
          },
          v2
        ]
      },
      {
        "kind": "LinkedHandle",
        "alias": null,
        "name": "allExactFishingPoints",
        "args": [
          {
            "kind": "Literal",
            "name": "last",
            "value": 10,
            "type": "Int"
          }
        ],
        "handle": "connection",
        "key": "Points_allExactFishingPoints",
        "filters": []
      }
    ]
  }
};
})();
(node/*: any*/).hash = '3acd4aab16445a78b74b8040a54319c1';
module.exports = node;
