import { commitMutation, graphql, } from 'react-relay'
// import { ConnectionHandler } from 'relay-runtime'
import environment from '../Environment'

const mutation = graphql`
  mutation CreateWorkerMutation($input: CreateWorkerInput!) {
    createWorker(input: $input) {
      newWorker {
        id
        name
        position
        experience
        description
        link
      }
    }
  }
`

export default (name, position, experience, description, link, callback ) => {
  const variables = {
    input: {
      worker: {
        name,
        position,
        experience,
        description,
        link,
      },
      clientMutationId: ""
    },
  }

  commitMutation(
    environment,
    {
      mutation,
      variables,
      onCompleted: () => {
        callback()
      },
      onError: err => console.error(err),
    },
  )
}
