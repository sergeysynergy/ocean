/**
 * @flow
 * @relayHash 5901fda5dbacd847d7bdba8f0ea82110
 */

/* eslint-disable */

'use strict';

/*::
import type {ConcreteBatch} from 'relay-runtime';
export type CreateWorkerMutationVariables = {|
  input: {
    worker?: ?{
      name: string;
      position?: ?string;
      experience?: ?string;
      description?: ?string;
      link?: ?string;
      order?: ?number;
    };
    clientMutationId?: ?string;
  };
|};
export type CreateWorkerMutationResponse = {|
  +createWorker: ?{|
    +newWorker: ?{|
      +id: string;
      +name: string;
      +position: ?string;
      +experience: ?string;
      +description: ?string;
      +link: string;
    |};
  |};
|};
*/


/*
mutation CreateWorkerMutation(
  $input: CreateWorkerInput!
) {
  createWorker(input: $input) {
    newWorker {
      id
      name
      position
      experience
      description
      link
    }
  }
}
*/

const batch /*: ConcreteBatch*/ = {
  "fragment": {
    "argumentDefinitions": [
      {
        "kind": "LocalArgument",
        "name": "input",
        "type": "CreateWorkerInput!",
        "defaultValue": null
      }
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "CreateWorkerMutation",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": [
          {
            "kind": "Variable",
            "name": "input",
            "variableName": "input",
            "type": "CreateWorkerInput!"
          }
        ],
        "concreteType": "CreateWorkerPayload",
        "name": "createWorker",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "args": null,
            "concreteType": "WorkerNode",
            "name": "newWorker",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "id",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "name",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "position",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "experience",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "description",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "link",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Mutation"
  },
  "id": null,
  "kind": "Batch",
  "metadata": {},
  "name": "CreateWorkerMutation",
  "query": {
    "argumentDefinitions": [
      {
        "kind": "LocalArgument",
        "name": "input",
        "type": "CreateWorkerInput!",
        "defaultValue": null
      }
    ],
    "kind": "Root",
    "name": "CreateWorkerMutation",
    "operation": "mutation",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": [
          {
            "kind": "Variable",
            "name": "input",
            "variableName": "input",
            "type": "CreateWorkerInput!"
          }
        ],
        "concreteType": "CreateWorkerPayload",
        "name": "createWorker",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "args": null,
            "concreteType": "WorkerNode",
            "name": "newWorker",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "id",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "name",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "position",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "experience",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "description",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "link",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "text": "mutation CreateWorkerMutation(\n  $input: CreateWorkerInput!\n) {\n  createWorker(input: $input) {\n    newWorker {\n      id\n      name\n      position\n      experience\n      description\n      link\n    }\n  }\n}\n"
};

module.exports = batch;
