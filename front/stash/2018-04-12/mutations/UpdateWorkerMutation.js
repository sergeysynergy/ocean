import { commitMutation, graphql, } from 'react-relay'
import environment from '../Environment'

const mutation = graphql`
  mutation UpdateWorkerMutation($input: UpdateWorkerInput!) {
    updateWorker(input: $input) {
      updatedWorker {
        id
        name
        position
        experience
        description
        order
      }
    }
  }
`

export default (id, name, position, experience, description, link, order, callback ) => {
  const variables = {
    input: {
      id,
      worker: {
        name,
        position,
        experience,
        description,
        link,
        order,
      },
    },
  }

  commitMutation(
    environment,
    {
      mutation,
      variables,
      onCompleted: () => {
        callback()
      },
      onError: err => console.error(err),
    },
  )
}
