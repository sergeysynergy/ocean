import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'

import environment from '../../Environment'
import PointList from './PointList'
/* End of import section ***************************************/

const PointListPageQuery = graphql`
  query PointListPageQuery {
    viewer {
      ...PointList_viewer
    }
  }
`
/* End of constants and variables section ****************************/


class PointListPage extends Component {
  render() {
    return <QueryRenderer
      environment={environment}
      query={PointListPageQuery}
      render={({error, props}) => {
        if (error) {
          return <div>{error.message}</div>
        } else if (props) {
            console.log('props', props);
            return <PointList viewer={props.viewer} />
        }
        return <div>Loading</div>
      }}
    />
  }
}


export default PointListPage
