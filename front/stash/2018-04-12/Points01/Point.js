import React, { Component } from 'react'
import { createFragmentContainer, graphql } from 'react-relay'


const isBrowser = typeof window !== 'undefined';
const RL = isBrowser ? require('react-leaflet') : undefined

/* End of import section ***************************************/

class Point extends Component {
  render() {
    return <RL.Circle
      key={this.props.point.id}
      center={[this.props.point.lat, this.props.point.lon]}
      radius={10}
      color={'green'}
    />
  }
}

export default createFragmentContainer(Point, graphql`
  fragment Point_point on ExactFishingPointNode {
    id
    lat
    lon
    datetime
  }
`)
