import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'

import environment from '../../Environment'
import RefetchPointItem from './RefetchPointItem'
/* End of import section ***************************************/


const RefetchPointQuery = graphql`
  query RefetchPointQuery($itemID: ID!) {
    trusted(id: $itemID) {
      ...RefetchPointItem_item
    }
  }
`
/* End of constants and variables section ****************************/


class RefetchPoint extends Component {
  state = {
    after: "",
  }

  _loadMore(after) {
    console.log('more', after);
    this.setState({after: after})
    // ... you'll implement this in a bit
  }

  render() {
    return <QueryRenderer
      environment={environment}
      query={RefetchPointQuery}
      variables={{
        itemID: "UG9pbnQ6Mg==",
        count: 10,
        after: this.state.after,
        from: "2015-02",
        till: "2015-03",
      }}
      render={({error, props}) => {
        console.log('props', props);
        if (error) {
          return <div>{error.message}</div>
        } else if (props) {
          console.log('hm', props);

          return <div>
            <RefetchPointItem item={props.point}/>
          </div>
        }
        return <div>Loading</div>
      }}
    />
  }
}


export default RefetchPoint
