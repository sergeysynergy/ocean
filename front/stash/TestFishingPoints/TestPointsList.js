import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'

import environment from '../../Environment'
import PointsListItem from './TestPointsListItem'
/* End of import section ***************************************/


const TestPointsListQuery = graphql`
  query TestPointsListQuery ($count: Int!, $after: String!) {
    allTrusted(
      first: $count
      after: $after
    ) @connection(
      key: "TestPoints_allTrusted",
      filters: []
    ) {
      edges {
        node {
          ...TestPointsListItem_point
        }
      }
      pageInfo {
        hasNextPage
        endCursor
      }
    }
  }
`
/* End of constants and variables section ****************************/


class Points extends Component {
  state = {
    after: "",
  }

  _loadMore(after) {
    console.log('more', after);
    this.setState({after: after})
    // ... you'll implement this in a bit
  }

  render() {
    return <QueryRenderer
      environment={environment}
      query={TestPointsListQuery}
      variables={{
        count: 10,
        after: this.state.after,
        from: "2015-02",
        till: "2015-03",
      }}
      render={({error, props}) => {
        console.log('props', props);
        if (error) {
          return <div>{error.message}</div>
        } else if (props) {
          console.log('hm', props);
          let after = props.allTrustedFishingPoints.pageInfo.endCursor
          let next = props.allTrustedFishingPoints.pageInfo.hasNextPage
          if (next) {
            // this._loadMore(after)
          }

          return <div>
            <hr />
            > {after}
            <br />
            {next}
            {next?
            <div className='flex ml4 mv3 gray'>
              <div className='pointer' onClick={() =>
                  this._loadMore(after)}
              >More</div>
            </div>
            :
            "no next"
            }
            <hr />

            {props.allTrustedFishingPoints.edges.map(({node}) => {
              return <PointsListItem
                key={node.__id}
                point={node}
              />
            })}

          </div>
        }
        return <div>Loading</div>
      }}
    />
  }
}


export default Points
