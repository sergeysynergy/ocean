/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteFragment } from 'relay-runtime';
import type { FragmentReference } from 'relay-runtime';
declare export opaque type TestPointsListItem_point$ref: FragmentReference;
export type TestPointsListItem_point = {|
  +id: string,
  +lat: number,
  +lon: number,
  +datetime: any,
  +$refType: TestPointsListItem_point$ref,
|};
*/


const node/*: ConcreteFragment*/ = {
  "kind": "Fragment",
  "name": "TestPointsListItem_point",
  "type": "TrustedNode",
  "metadata": null,
  "argumentDefinitions": [],
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "id",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "lat",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "lon",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "datetime",
      "args": null,
      "storageKey": null
    }
  ]
};
(node/*: any*/).hash = '9e090a5471df04efbcdbc3f10d50916e';
module.exports = node;
