import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'

import environment from '../../Environment'
import Point from './Point'
/* End of import section ***************************************/


const query = graphql`
  query PointRendererQuery ($q: String!) {
    bulkTrusted(q: $q) {
      ...Point_point
    }
  }
`
/*
  query PointRendererQuery ($count: Int!, $after: String!) {
    allTrusted(
      first: $count
      after: $after
    ) @connection(
      key: "Trusted_allTrusted",
      filters: []
    ) {
      edges {
        node {
          ...Point_point
        }
      }
      pageInfo {
        hasNextPage
        endCursor
      }
    }
  }
*/
/* End of constants and variables section ****************************/


class Points extends Component {
  state = {
    after: "",
  }

  _loadMore(after) {
    console.log('more', after);
    this.setState({after: after})
    // ... you'll implement this in a bit
  }

  render() {
    return <QueryRenderer
      environment={environment}
      query={query}
      variables={{
        q: "test",
        count: 10,
        after: this.state.after,
        from: "2015-02",
        till: "2015-03",
      }}
      render={({error, props}) => {
        console.log('props', props);

        if (error) {
          return <div>{error.message}</div>
        } else if (props) {

          return <div>
            Points
            {
              props.bulkTrusted.map((node) => {
                return <Point
                  key={node.__id}
                  point={node}
                />
              })
            }
          </div>
        }
        return <div>Loading</div>
      }}
    />
  }
}


export default Points
