import React, { Component } from 'react'
import {createRefetchContainer, graphql} from 'react-relay';
import { Input, Label, Button } from 'reactstrap'

class RefetchPointItem extends Component {
  render() {
    const item = this.props.item;
    return <div>
      {' '}{item.id}
      <Button onClick={this._refetch}>Refetch</Button>
    </div>
  }

  _refetch = () => {
    this.props.relay.refetch(
      {itemID: this.props.item.id},  // Our refetchQuery needs to know the `itemID`
      null,  // We can use the refetchVariables as renderVariables
      () => { console.log('Refetch done') },
      {force: true},  // Assuming we've configured a network layer cache, we want to ensure we fetch the latest data.
    );
  }
}

export default createRefetchContainer(
  RefetchPointItem,
  graphql`
    fragment RefetchPointItem_item on TrustedNode {
      id
      datetime
      lat
      lon
    }
  `,
  graphql`
    # Notice that we re-use our fragment and the shape of this query matches our fragment spec.
    query RefetchPointItemRefetchQuery($itemID: ID!) {
      trusted(id: $itemID) {
        ...RefetchPointItem_item
      }
    }
  `
);
