import os
import json
from datetime import datetime
from django.utils import timezone
import h5py
import numpy as np
from netCDF4 import Dataset

from app.helpers import dayToDate, getNetCDFMetaData


# Just python speed test function
def speedTest():
    res = 0
    for n in range(10000000):
        res = res + n
    print('speed test res:', res, '\n')
    return True

"""
# Create datetime from string
strng = '2015-01-09 00:00:00'
dt = datetime.strptime(strng, '%Y-%m-%d %H:%M:%S')
dt = timezone.make_aware(dt, timezone.get_current_timezone())
print('dt: ', dt)

# Parse hdf for region bounds
def getNetCDFBounds(path, ncTag):
    print('parsing file: ', path)
    # speedTest()
    if os.path.exists(path):
        print('looking bounds for: ' + ncTag + '\n')
        f = h5py.File(path, 'r')
        navData = f['navigation_data']
        # lat = navData.getitem('latitude')
        lat = navData['latitude']
        lon = navData['longitude']

        print('>>', lat[0,0])
"""


def readtolist(dataset):
    return "normal list count = {0}".format(len(list(dataset['1'])))

def readtonp(dataset):
    n1 = np.array(dataset)
    return "numpy count = {0}".format(len(n1))

# path = '/data/parsing/test/01_SST.nc'
path = '/data/parsing/test/72points_OC.nc'
ncTag = 'chlor_a'
meta = getNetCDFMetaData(path)
print('date: %s'%(meta['date']))
