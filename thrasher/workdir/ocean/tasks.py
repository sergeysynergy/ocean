from __future__ import absolute_import, unicode_literals
import os
import datetime
from celery import shared_task
from celery.utils.log import get_task_logger
from django.utils import timezone

from app.datetime import timezonedDateTime
# from ocean.fishingpoints import parseCSV
# from chlorophyll.tasks import parseChlorophyll
# print("OCEAN TASKS")


# Initialize environment before parsing
def init():
    # Task logger
    logger = get_task_logger(__name__)

    # Check if fishing points directori exists, create if needed
    if not os.path.exists(FISHING_POINTS_DIR):
        os.makedirs(FISHING_POINTS_DIR)

    # Create directories structure of trusted fishing points
    # createParseDirTree(FISHING_POINTS_DIR + '/TrustedFishingPoint')

    # Create directories structure of expected fishing points
    # createParseDirTree(FISHING_POINTS_DIR + '/Expectedfishingpoint')


# Bulk insert fishing points in DB of given data and model
@shared_task
def createFishingPointsTask(data, model):
    # logger.info("bulk creating data points...")
    instances = [
        model(
            lat = float(row['lat']),
            lon = float(row['lon']),
            datetime = timezonedDateTime(row['datetime']),
        )
        for row in data
    ]
    model.objects.bulk_create(instances)
    # logger.info("finished bulk creating data points")


# Looking for .csv files in FISHING_POINTS_DIR + model directory
# and parsing them
@shared_task
def parseCSV(model):
    init()
    for f in os.listdir(FISHING_POINTS_DIR + '/' + model):
        fyle = os.path.join(FISHING_POINTS_DIR + '/' + model, f)
        if os.path.isfile(fyle) and os.path.splitext(fyle)[1] == '.csv':
            """ parsing file """
            logger.info('parsing file: ' + str(fyle) + '...')
            name = (f + '-' + str(datetime.now())).replace(' ', '')
            inprogress = FISHING_POINTS_DIR + '/' + model + '/inprogress/' + name
            parsed = FISHING_POINTS_DIR + '/' + model + '/parsed/' + name
            # Move file to 'inprogress' directory while parsing
            os.rename(fyle, inprogress)
            # Call to parse csv file and store extracted data in cache
            cacheSets = parseCSVandCache(inprogress)
            # Moved parsed file to 'parsed' directory
            os.rename(inprogress, parsed)
            # os.rename(inprogress, fyle)
            logger.info('finish parsing file: ' + str(fyle))

            """ insert data from cache to DB """
            # If parsing successful:
            if cacheSets:
                for set in cacheSets:
                    logger.info('inserting set: '+set+' to DB')
                    data = fromBytesToDict(Cache.get(set))
                    Cache.delete(set)
                    # createFishingPointsTask.delay(data, 'TrustedFishingPoint')
                    createFishingPointsTask.s(
                        data,
                        model,
                    ).apply_async(queue='django')
                    logger.info('finished inserting set')
