from django.apps import AppConfig


class TrustedConfig(AppConfig):
    name = 'trusted'
