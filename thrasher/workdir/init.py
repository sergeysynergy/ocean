#!/usr/bin/python
import os
import sys
from osgeo import ogr
from netCDF4 import Dataset
import numpy as np
import numpy.ma as ma
import h5py
#
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "app.settings")
try:
    import django
except ImportError as exc:
    raise ImportError(
        "Couldn't import Django. Are you sure it's installed and "
        "available on your PYTHONPATH environment variable? Did you "
        "forget to activate a virtual environment?"
    ) from exc
django.setup()  # Setup Django env for testing
#
#
print('Project env initialized')
