"""
from __future__ import absolute_import, unicode_literals
from celery import shared_task
from celery.utils.log import get_logger
#
from app.parsenetcdf import parseNetCDF
#
logger = get_logger(__name__)
# print("SST TASKS")


@shared_task
def parseAndInsertSST(metadata, filepath, tag):
    # Tag in NetCDF file of desired dataset
    ncTag = 'sst'

    # Call to parse csv file and store extracted data in cache
    result = parseNetCDF(filepath, metadata, ncTag)

    # Append django data model name to metadata for futher processing
    result["metadata"]["model"] = "SeaSurfaceTemperature"

    return result
"""
