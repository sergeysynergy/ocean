import os


def joinList(iterator, seperator):
    """Join list using seperator"""
    it = map(str, iterator)
    seperator = str(seperator)
    string = next(it, '')
    for s in it:
        string += seperator + s
    return string


def sortDirsByTime(path, limit=False):
    """Get directories sorted by time, limit number if needed"""
    # Add closing slash to path
    path = '%s/' % (path)
    a = [path + s for s in os.listdir(path)
         if os.path.isdir(os.path.join(path, s))]
    a.sort(key=lambda s: os.path.getmtime(os.path.join(path, s)))
    a.reverse()
    if limit:
        a = a[0:limit]

    return a


def sortFilesByTime(path, ext=False, limit=False):
    """Get sorted files by time in given directory, filter
    by extension and limit number if need
    """
    a = []
    for s in os.listdir(path):
         if os.path.isfile(os.path.join(path, s)):
            if ext:
                if os.path.splitext(s)[1] == ext:
                    a.append(path + '/' + s)
            else:
                a.append(path + '/' + s)

    a.sort(key=lambda s: os.path.getmtime(os.path.join(path, s)))
    a.reverse()
    if limit:
        a = a[0:limit]

    return a


def getFilenameExt(filepath):
    """Get filename and extension from filepath"""
    ext = os.path.splitext(filepath)[1]
    filename = os.path.splitext(filepath)[0].split('/')[-1]
    return filename, ext


def getFilename(filepath):
    """Get filename, extension and directory path from filepath"""
    ext = os.path.splitext(filepath)[1]
    filename = os.path.splitext(filepath)[0].split('/')[-1]
    dirpath = joinList(filepath.split('/')[0:-1], '/')
    return {
        'name': filename,
        'ext': ext,
        'dirpath': dirpath,
    }


def getFileName(filepath):
    """Get filename, extension and directory path from filepath"""
    ext = os.path.splitext(filepath)[1]
    filename = os.path.splitext(filepath)[0].split('/')[-1]
    dirpath = joinList(filepath.split('/')[0:-1], '/')
    return filename, ext, dirpath
