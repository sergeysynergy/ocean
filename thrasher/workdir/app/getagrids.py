import os
import numpy as np
import numpy.ma as ma
import logging
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
#
from app.settings import TESTMODE, GRIDS


logger = logging.getLogger(__name__)
# Create directory to store grids
outputdir = GRIDS
if not os.path.exists(outputdir):
    os.makedirs(outputdir)


def getaGrids():
    """Create explicit grid of region, save point to CSV file"""
    logger.info('Creating region grids for futher interpolation...')

    def contains(point, polygon):
        """Calculate the occurrence of a point in a polygon"""
        point = Point(point)
        return polygon.contains(point)

    def getNorvSea():
        """Create Norv Sea data grid"""
        # Norv Sea polygon (longitude, latitude)
        polygon = Polygon([
            (5.897592, 59.453594),
            (-15.743695, 64.412255),
            (15.617894, 77.781198),
            (27.955703, 70.515584),
            (5.897592, 59.453594),
        ])

        regionSlug = 'norv-sea'

        if TESTMODE:  # 4 testing
            regionSlug = 'test-norv-sea'
            glats = np.linspace(59.45, 77.78, 100)
            glons = np.linspace(-15.74, 27.96, 200)
        else:
            glats = np.linspace(59.45, 77.78, 1652)
            glons = np.linspace(-15.74, 27.96, 2035)

        filepath = '%s/%s.csv' % (outputdir, regionSlug)
        # Create grid if file not exists:
        if not os.path.exists(filepath):
            # Get lons/lats grids using numpy meshgrid function
            gridLons, gridLats = np.meshgrid(glons, glats)
            # Plane and stack arrays in one
            square = np.column_stack((gridLons.ravel(), gridLats.ravel()))

            # Creating numpy mask for points belongs to polygon:
            size = square.shape[0]
            mask = ma.make_mask(square[:, [0]])
            for key in range(size):
                if contains(square[key], polygon):
                    mask[key] = [False]

            # Applay mask to rough latitude and longitude values
            mlats = ma.masked_array(square[:, 1], mask=mask)
            mlons = ma.masked_array(square[:, 0], mask=mask)

            # Create resulting array using mask mechanics
            result = np.column_stack((mlats.compressed(), mlons.compressed()))

            # Save result as CSV file
            np.savetxt('%s/%s.csv' % (outputdir, regionSlug), result)

    def getKurils():
        regionSlug = 'kurils'

        # Grid data for Kurils interpolation:
        if TESTMODE:  # 4 testing
            regionSlug = 'test-kurils'
            glats = np.linspace(39.98, 43.90, 10)
            glons = np.linspace(145.93, 151.67, 10)
        else:
            glats = np.linspace(39.98, 43.90, 436)
            glons = np.linspace(145.93, 151.67, 449)

        filepath = '%s/%s.csv' % (outputdir, regionSlug)
        # Create grid if file not exists:
        if not os.path.exists(filepath):
            gridLons, gridLats = np.meshgrid(glons, glats)

            # Plane lat/lons arrays, stack them in one numpy array
            result = np.column_stack((gridLats.ravel(), gridLons.ravel()))

            # Save result as CSV file
            np.savetxt('%s/%s.csv' % (outputdir, regionSlug), result)

    getNorvSea()
    getKurils()

    logger.info('Finish creating region grids')
