from django.test import TestCase

# Create your tests here.
PARSING_DIR = '/repo/ingest/parseandinsert.bit'

# Looking for .nc files in PARSING_DIR with "CLH" in file name and parsing them
@shared_task(name="Parsing .nc files for chlorophyll data")
def parseChlorophyll():
    model = 'Chlorophyll'  # data model
    logger = get_task_logger(__name__)  # task logger

    for f in os.listdir(PARSING_DIR):
        print("FYLE", f)
        fyle = os.path.join(PARSING_DIR, f)
        if os.path.isfile(fyle) and os.path.splitext(fyle)[1] == '.nc':
            """ parsing file """
            logger.info('parsing file: ' + str(fyle) + '...')
            name = (f + '-' + str(datetime.now())).replace(' ', '')
            inprogress = PARSING_DIR + '/' + '/inprogress/' + name
            parsed = PARSING_DIR + '/' + '/parsed/' + name
            # Move file to 'inprogress' directory while parsing
            os.rename(fyle, inprogress)
            # Call to parse csv file and store extracted data in cache
            cacheSets = parseNetCDFandCache(inprogress)
            # Moved parsed file to 'parsed' directory
            os.rename(inprogress, parsed)
            logger.info('finish parsing file: ' + str(fyle))

            """ insert data from cache to DB """
            # If parsing successful:
            if cacheSets:
                for set in cacheSets:
                    logger.info('inserting set: '+set+' to DB')
                    data = fromBytesToDict(Cache.get(set))
                    Cache.delete(set)
                    # createDataPointsTask.delay(data, 'TrustedFishingPoint')
                    createDataPointsTask.s(
                        data,
                        model,
                    ).apply_async(queue='django')
                    logger.info('finished inserting set: '+set)
