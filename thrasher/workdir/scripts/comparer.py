#!/usr/bin/python
import os
import sys
from scipy.interpolate import griddata
from osgeo import ogr
from netCDF4 import Dataset
import numpy as np
import numpy.ma as ma
import h5py
from django.contrib.gis.geos import Polygon, MultiPolygon
from random import randint
#
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "app.settings")
try:
    import django
except ImportError as exc:
    raise ImportError(
        "Couldn't import Django. Are you sure it's installed and "
        "available on your PYTHONPATH environment variable? Did you "
        "forget to activate a virtual environment?"
    ) from exc
print('Setup django environment...')
django.setup()  # Setup Django env for testing
print('Done.\nRunning tests...')
#
from app.settings import TESTMODE, DEBUG
from app.parsenetcdf import parseNetCDF
from app.helpers import checkBoundsHit, getNetCDFMetaData, timer, quickHull
from ocean.models import Region
#
#

def parseAndRandompolate(filepath, vlons, vlats):
    """Parse data from NetCDF and interpolate it using numpy methods"""
    result = parseNetCDF(filepath, 'chlor_a')
    print(result)
    # Load geo data from file into numpy arrya
    dataset = np.loadtxt(result['output'])
    # Extract data from numpy array as needed for futher interpolation:
    lats = dataset[0:, 0]
    lons = dataset[0:, 1]
    vals = dataset[0:, 2]
    points = np.column_stack((lats, lons))
    # Do interpolation using numpy methods
    grid = griddata(points, vals, (vlons, vlats), method='nearest')
    # Gether all data in one numpy array with lats, lons and vals
    result = np.column_stack((vlats.ravel(), vlons.ravel(), grid.ravel()))
    # Get quick hull of processed data
    hull = quickHull(np.column_stack((vlats.ravel(), vlons.ravel())))
    print(hull)
    # Save result to .csv file
    np.savetxt('/parsing/test/random_numpy.norvsea.csv', result)


def parseAndInterpolateWithNumpy(filepath, vlons, vlats):
    """Parse data from NetCDF and interpolate it using numpy methods"""
    result = parseNetCDF(filepath, 'chlor_a')
    print(result)
    # Load geo data from file into numpy arrya
    dataset = np.loadtxt(result['output'])
    # Extract data from numpy array as needed for futher interpolation:
    lats = dataset[0:, 0]
    lons = dataset[0:, 1]
    vals = dataset[0:, 2]
    points = np.column_stack((lats, lons))
    inputHull = quickHull(np.column_stack((lats, lons)))
    grid = griddata(points, vals, (vlons, vlats), method='nearest')
    # Gether all data in one numpy array with lats, lons and vals
    result = np.column_stack((vlats.ravel(), vlons.ravel(), grid.ravel()))
    outputHull = quickHull(np.column_stack((vlats.ravel(), vlons.ravel())))
    # print(outputHull)
    np.savetxt('/parsing/test/inter_numpy.norvsea.csv', result)


def parseAndRandompolate(filepath, vlons, vlats):
    """Parse data from NetCDF and interpolate it using numpy methods"""
    result = parseNetCDF(filepath, 'chlor_a')
    print(result)
    # Load geo data from file into numpy arrya
    dataset = np.loadtxt(result['output'])
    # Extract data from numpy array as needed for futher interpolation:
    lats = dataset[0:, 0]
    lons = dataset[0:, 1]
    vals = dataset[0:, 2]
    points = np.column_stack((lats, lons))
    inputHull = quickHull(np.column_stack((lats, lons)))
    # print(inputHull)

    valsNumber = vals.shape[0]
    rlats = vlats.ravel()
    rlons = vlons.ravel()
    shape = rlats.shape[0]
    rvals = np.arange(shape)
    for k in range(shape):
        r = randint(0, valsNumber-1)
        value = vals[r]
        rvals[k] = rvals[k] * 3

    result = np.column_stack((rlats, rlons, rvals))
    outputHull = quickHull(np.column_stack((rlats, rlons)))
    # print(outputHull)
    np.savetxt('/parsing/test/random_numpy.norvsea.csv', result)


# Main
if __name__ == "__main__":
    # filepath = '/parsing/input/T2016265105500.L2_LAC_OC.nc'
    filepath, geotag = '/parsing/test/72points_OC.nc', 'chlor_a'
    # filepath, geotag = '/parsing/input/T2016265005500.L2_LAC_SST.nc', 'sst'
    region = Region.objects.get(slug='default')
    metadata = getNetCDFMetaData(filepath)
    if not checkBoundsHit(region, metadata['boundsWKT']):
        print("Region not match!")

    root = Dataset(filepath, 'r', format='NETCDF4')
    lines = root.dimensions['number_of_lines'].size  # X axis
    pixels = root.dimensions['pixels_per_line'].size  # Y axis
    lats = root['navigation_data']['latitude'][:]
    lons = root['navigation_data']['longitude'][:]
    dataset = root['geophysical_data'][geotag]
    # print(dataset)

    # Define region grid
    glats = np.linspace(59.45, 77.78, 165)  # 1652
    glons = np.linspace(-15.74, 27.96, 203)  # 2035
    vlons, vlats = np.meshgrid(glons, glats)

    print('\n Testing numpy interpolation:')
    timer(parseAndInterpolateWithNumpy(filepath, vlons, vlats))

    print('\n Testing simple operation with numpy array - random assignment:')
    timer(parseAndRandompolate(filepath, vlons, vlats))

    """
    v = dataset[:]
    # print(v.shape,"\n")

    # print(v.mask)
    # ma.masked_outside(values, 0.1, 0.9)
    # print(v[~v.mask])
    c = v.compressed()
    print(c.shape,"\n")

    mlats = ma.masked_array(lats, mask=v.mask)
    print(mlats.shape,"\n")
    """
