#!/usr/bin/python
"""Create grids for interpolation using projection"""
# from mpl_toolkits.basemap import Basemap
import numpy as np
# from matplotlib.path import Path
from shapely.geometry import Polygon, Point
# from shapely.geometry import MultiPoint
#
#
# Longitude, then latitude
norvSea = [
    [5.897592, 59.453594],
    [-15.743695, 64.412255],
    [15.617894, 77.781198],
    [27.955703, 70.515584],
    [5.897592, 59.453594]
]

def withProjection():
    # Mercator Projection
    # http://matplotlib.org/basemap/users/merc.html
    m = Basemap(projection='merc', llcrnrlat=-80, urcrnrlat=80,
                llcrnrlon=-180, urcrnrlon=180, lat_ts=20, resolution='c')

    # Poly vertices
    p = [[25.774252, -80.190262], [18.466465, -66.118292], [32.321384, -64.75737]]

    # Projected vertices
    p_projected = [m(x[1], x[0]) for x in p]

    # Create the Path
    p_path = Path(p_projected)

    # Test points
    p1 = [27.254629577800088, -76.728515625]
    p2 = [27.254629577800088, -74.928515625]

    # Test point projection
    p1_projected = m(p1[1], p1[0])
    p2_projected = m(p2[1], p2[0])

    if __name__ == '__main__':
        print(p_path.contains_point(p1_projected))  # Prints 1
        print(p_path.contains_point(p2_projected))  # Prints 1


def withShapely():
    """Create region grid using Shapely lib"""
    print('Create grid using Shapely lib')
    # Declare region of interest
    poly = Polygon(norvSea)
    # Create grid using max and min values of longitude and latitude
    lons, lats = np.mgrid[-15.74:27.96:20j, 59.45:77.78:16j]
    # Create empty numpy array same shape as lons/lats to use as mask later
    mask = np.zeros(shape=(20, 16))

    # lats = np.loadtxt('kurils.csv')[0:, 0]
    # lons = np.loadtxt('kurils.csv')[0:, 1]
    # glats = np.linspace(60, 70, 100)
    # glons = np.linspace(-15, 15, 200)
    # glats = np.linspace(59.45, 77.78, 1652)
    # glons = np.linspace(-15.74, 27.96, 2035)
    # vlons, vlats = np.meshgrid(glons, glats)
    # poly = MultiPoint(grid).convex_hull

    # Check and store only grid points in polygon - region of interest
    for y in range(20):
        for x in range(16):
            lat = lats[y, x]
            lon = lons[y, x]
            point = Point(lon, lat)
            if poly.contains(point):
                mask[y, x] = True
                pass
                # print('Hit - lat: %s, lon: %s' % (lat, lon))
            else:
                pass
                mask[y, x] = False
                # print('Miss - lat: %s, lon: %s' % (lat, lon))

    print(mask)
    lons = np.ma.array(lons, mask=mask)
    lats = np.ma.array(lats, mask=mask)
    print(lons)
    print(lons[~lons.mask])
    print(lons.compressed())


withShapely()
