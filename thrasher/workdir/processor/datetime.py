import time
import datetime
#
#
# >>> datetime.datetime.utcfromtimestamp(1347517370).strftime('%Y-%m-%d %H:%M:%S')
# datetime.datetime.fromtimestamp(1347517370).strftime('%c')
# datetime.datetime.fromtimestamp(1347517370).strftime('%Y-%m-%d %H:%M:%S')
# time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(1347517370))
# int(time.strftime('%Y', time.localtime(1347517370)))
# date = '1900-01-01'
# tStamp = int(time.mktime(datetime.datetime.strptime(date, "%Y-%m-%d").timetuple()))

# epoch = '1970-01-01'

def getNetcdfEpochTimeDelta():
    """Get delta from NetCDF epoch time (1900-01-01) to unix
    epoch time (1970-01-01)
    """
    date = '1900-01-01'
    delta = int(time.mktime(
                datetime.datetime.strptime(date, "%Y-%m-%d").timetuple()))
    return delta

def netcdfTimeToDatetimeList(netcdfTime):
    # Generate timestamp
    timeStamp = netcdfTime * 3600 + getNetcdfEpochTimeDelta()

    # Convert timestamp to date values list
    # datetime.datetime.fromtimestamp(timeStamp).strftime('%Y-%m-%d %H:%M:%S')
    dt = []
    dt.append(datetime.datetime.fromtimestamp(timeStamp).strftime('%Y'))
    dt.append(datetime.datetime.fromtimestamp(timeStamp).strftime('%m'))
    dt.append(datetime.datetime.fromtimestamp(timeStamp).strftime('%d'))
    dt.append(datetime.datetime.fromtimestamp(timeStamp).strftime('%H'))
    dt.append(datetime.datetime.fromtimestamp(timeStamp).strftime('%M'))
    dt.append(datetime.datetime.fromtimestamp(timeStamp).strftime('%S'))

    return dt
