import ast
import json
import os
import calendar
import numpy as np
from netCDF4 import Dataset
from datetime import datetime, timedelta
import numpy
from osgeo import ogr
import math
import time
#
from app.helpers import getFileName


def yearMonthWithLastMonthDay(date):
    """Convert [year-month] string to [year-month-lastMonthDay]"""
    year = int(date.split('-')[0])
    month = int(date.split('-')[1])
    lastDay = calendar.monthrange(year, month)[1]
    endDate = date + '-' + str(lastDay)

    return endDate


def dayToDate(year, day):
    """Convert day of the given year to date format"""
    dt = datetime(year, 1, 1)
    dtdelta = timedelta(days=(day-1))
    date = dt + dtdelta

    return date

# Parse netCDF for meta data
def getNetCDFMetaData(filepath):
    meta = {
        'boundsLatLon': [],   # latLng multipolygon with nav region bounds
        'boundsWKT': '',   # region bounds in WKT format
        'year': '',     # scan year
        'day': '',      # scan day
        'lines': 0,     # scan lines number
        'pixels': 0,    # scan pixels number
    }
    # print('parsing file for meta data: ', path)
    # speedTest()
    if os.path.exists(filepath):
        root = Dataset(filepath, 'r', format='NETCDF4')  # root of NetCDF file
        lines = root.dimensions['number_of_lines'].size  # X axis
        pixels = root.dimensions['pixels_per_line'].size  # Y axis
        nd = root['navigation_data']
        # lat = root['navigation_data']['latitude']
        # lon = root['navigation_data']['longitude']
        # times = root['navigation_data'][:]

        # Getting bounds data
        boundsLatLon = []
        boundsWKT = 'POLYGON (('
        for key in nd.gringpointsequence:
            boundsLatLon.append([
                nd.gringpointlatitude[key-1],
                nd.gringpointlongitude[key-1],
                ])
            x, y = latLon2wkt(
                        nd.gringpointlatitude[key-1],
                        nd.gringpointlongitude[key-1],
                        )
            boundsWKT += "%s %s, " % (x, y)
        x, y = latLon2wkt(nd.gringpointlatitude[0], nd.gringpointlongitude[0])
        boundsWKT += "%s %s))" % (x, y)

        # Get date of data recording
        year = int(root['scan_line_attributes']['year'][0])
        day = int(root['scan_line_attributes']['day'][0])
        date = str(dayToDate(year, day))[:10]

        # Fill meta data
        meta['year'] = year
        meta['day'] = day
        meta['date'] = date
        meta['boundsLatLon'] = str(boundsLatLon)
        meta['boundsWKT'] = boundsWKT
        meta['lines'] = lines
        meta['pixels'] = pixels
        meta['lats'] = str(nd['latitude'][0, 0])
        meta['lons'] = str(nd['longitude'][0, 0])

        """
        try:
            f = h5py.File(filepath, 'r')
            # zroot = f['navigation_data']
            # meta['WENEEDIT'] = str(nd.getncattr('add_offset'))
            meta['WENEEDIT'] = str(f.geospatial_lat_max)
        except:
            meta['WENEEDIT'] = 'null'
        """

        # print('meta', meta)
        return meta
    else:
        print('file %s not found' % (filepath))
        return False

# Convert data from Memcached server to python dict structure
def fromBytesToList(bts):
    # Convert bytes to utf string
    zstr = str(bts, 'utf-8')
    # Convert string representation of dict to dict
    data = ast.literal_eval(zstr)

    return data

# quickhull2d.py (modified)
# This is a pure Python version of the Quick Hull algorithm.
# It's based on the version of literateprograms, but fixes some
# old-style Numeric function calls.
# This version works with numpy version 1.2.1
# See: http://en.literateprograms.org/Quickhull_(Python,_arrays)
# Modified by Wim Bakker 20081210
def quickHull(sample):
    link = lambda a,b: numpy.concatenate((a,b[1:]))
    edge = lambda a,b: numpy.concatenate(([a],[b]))

    def dome(sample,base):
        h, t = base
        dists = numpy.dot(sample-h, numpy.dot(((0,-1),(1,0)),(t-h)))
        outer = numpy.repeat(sample, dists>0, axis=0)

        if len(outer):
            pivot = sample[numpy.argmax(dists)]
            return link(dome(outer, edge(h, pivot)),
                        dome(outer, edge(pivot, t)))
        else:
            return base

    if len(sample) > 2:
        axis = sample[:,0]
        base = numpy.take(sample, [numpy.argmin(axis), numpy.argmax(axis)], axis=0)
        return link(dome(sample, base),
                    dome(sample, base[::-1]))
    else:
        return sample


# Encode numpy array to json
class NumpyAwareJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            if obj.ndim == 1:
                return obj.tolist()
            else:
                return [self.default(obj[i]) for i in range(obj.shape[0])]
        return json.JSONEncoder.default(self, obj)


# Absorbs data lat / long and turns into WKT used below
def latLon2wkt(lat, lon):
    halfCirc = 20037508.34
    x = lon * halfCirc / 180
    y = math.log(math.tan((90 + lat) * math.pi / 360)) / (math.pi / 180)
    y = y * halfCirc / 180
    return x, y


# Check crossing of two polygons
def checkBoundsHit(region, boundsWKT):
    try:
        # Make default region (multipolygon) match ogr WKT format
        defaultWkt = str(region.mpoly).strip().split(';')[1]

        # Create polygons from data in WKT format
        poly1 = ogr.CreateGeometryFromWkt(defaultWkt)
        poly2 = ogr.CreateGeometryFromWkt(boundsWKT)

        # Get polygons intersection result
        result = poly1.Intersection(poly2)

        # Check if intersection result is empty:
        if str(result).find('EMPTY') != -1:
            return False
        else:
            return True
    except Exception:
        return False


# Merging two dictionaries
def mergeTwoDicts(x, y):
    """Given two dicts, merge them into a new dict as a shallow copy."""
    z = x.copy()
    z.update(y)
    return z


# Convert list of latitudes and longitudes in string format to list
def fromStringToLanLongsList(bounds):
    bounds = "%s, " % (bounds[1:-1])
    bounds = bounds.split('], ')[:-1]
    result = []
    for row in bounds:
        result.append(row[1:].split(', '))
    return result


# Delete files in given list
def deleteFilesInList(filesList):
    for fyle in filesList:
        if os.path.isfile(fyle):
            os.remove(fyle)
    return True


def timer(func):
    """Decorator for counting the time of execution of functions"""
    start = time.time()
    # Call function from attributes
    func
    end = time.time()
    # Calculate and print elapsed time
    diff = end - start
    print('Seconds elapsed: ', diff)


def traitExtCheck(func):
    """Check for proper trait tag in filename and file extension"""
    def wrapper(*args, **kwargs):
        filepath = args[0]
        trait = args[1]
        geotag = args[2]
        ext = args[3]
        # Extract filename and filename from filepath
        filename, fileext, _ = getFileName(filepath)
        # EXIT CONDITION: if not proper extension
        if ext != fileext:
            return {
                'result': False,
                'info': 'File has wrong extension: %s' % filepath,
            }
        fileTraits = filename.split('_')
        # EXIT CONDITION: if proper trait tag not found in filename
        if trait not in fileTraits:
            return {
                'result': False,
                'info': 'Proper trait tag %s not found in filename: %s' % (
                    trait, filepath)
            }
        return func(filepath, trait, geotag)
    return wrapper
