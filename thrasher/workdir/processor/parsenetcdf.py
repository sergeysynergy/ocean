from __future__ import absolute_import, unicode_literals
from celery import shared_task
import logging
from netCDF4 import Dataset
import numpy as np
import numpy.ma as ma
# import pandas as pd
# from random import randint
#
from app.settings import TESTMODE
from ocean.models import Region
#
from .helpers import dayToDate


logger = logging.getLogger(__name__)
logger.info("PARSENETCDF TASKS")


@shared_task
def parseNetCDF(filepath, geotag, output=''):
    """Async extraction of the geophysical dataset from NetCDF file
    by given start and stop limits"""
    # Generating output filename if outputfile omitted:
    if output == '':
        output = "%s.parsed.%s.csv" % (filepath, geotag)
    # Remove possible spacers in output filepath
    output = output.replace(' ', '_')
    # Define root dataset object for given NetCDF file
    root = Dataset(filepath, 'r', format='NETCDF4')

    # EXIT CONDITION:
    # Trying to extract desired geophysical dataset values as numpy array:
    try:
        # Get dataset as numpy array
        values = root['geophysical_data'][geotag][:]
    except Exception:
        logger.warning('Proper dataset in file not found')
        return {
            'result': False,
            'info': 'Proper dataset for geotag %s not found in file: %s' %
                    (geotag, filepath),
            }

    # Handle time of recording
    # msec = root['scan_line_attributes']['msec'][:]
    # hours = msec / (1000*60*60)  # % 24

    # Latitudes as numpy array
    lats = root['navigation_data']['latitude'][:]
    # Longitudes as numpy array
    lons = root['navigation_data']['longitude'][:]

    # Trying to get numpy mask for values numpy array:
    try:
        mask = values.mask
        # Apply mask to lats array
        mlats = ma.masked_array(lats, mask=mask)
        # Apply mask to lons array
        mlons = ma.masked_array(lons, mask=mask)
    except Exception:
        # No mask found for values numpy array
        mlats = lats
        mlons = lons

    # Remove unnecessary values using numpy masking mechanics,
    # also flattening numpy arrays:
    clats = mlats.compressed()
    clons = mlons.compressed()
    cvalues = values.compressed()

    # Get number of data points (with not NaN values)
    points = cvalues.shape[0]

    # EXIT CONDITION:
    # Return false, if no any proper point found
    if points == 0:
        logger.warning('No points found after masking')
        return {
            'result': False,
            'info': 'No points found after masking for tag %s in file: %s' %
                    (geotag, filepath),
            }

    # Stack all required data in one numpy array
    result = np.column_stack((clats, clons, cvalues))

    # 4 testing:
    if TESTMODE:
        logger.warning('TOTAL POINTS %s' % values.ravel().shape)
        sample = 10000
        np.savetxt(output, result[:sample])
    # Production way:
    else:
        # Write result array to files
        np.savetxt(output, result)

    # Scan year
    year = int(root['scan_line_attributes']['year'][0])
    # Scan day of year
    day = int(root['scan_line_attributes']['day'][0])
    # Scan date in Y:m:d format
    date = dayToDate(year, day)

    return {
        'result': True,
        'info': 'NetCDF data extracted and written to file: %s' % (output),
        'output': output,
        'date': date,
        'points': points,
        }
