import os
import json
import numpy as np
from celery import shared_task
#
from app.settings import Cache
#
from .helpers import fromBytesToList, quickHull, NumpyAwareJSONEncoder
from .helpers import getNetCDFMetaData, fromStringToLanLongsList


@shared_task
def preCSVMetadata(filepath, tag):
    """Get metadata before file parsing, write metadata to metafile
    and return metadata for futher processing"""
    # Path to metafile
    metafile = '%s.%s.meta' % (filepath, tag)
    # Some metadata
    metadata = {
        tag: {},
    }
    # Write metadata to metafile
    with open(metafile, "a") as f:
        f.write(json.dumps(metadata, ensure_ascii=False))

    return metadata


@shared_task
def preHullMetadata(bounds, filepath, tag):
    # Reading metadata from existing metafile:
    metafile = '%s.%s.meta' % (filepath, tag)
    # Define metadata dictionary
    metadata = {}
    # Convert lat/lon data from sting to list
    latLons = fromStringToLanLongsList(bounds)
    # Number of parsed points
    points = len(latLons)
    # Create empty numpy array of points
    arr = np.zeros(shape=(points, 2))
    # Fill numpy array with geo data:
    counter = 0
    for row in latLons:
        if counter < points:
            arr[counter, 0] = row[0]
            arr[counter, 1] = row[1]
            counter += 1
    # Create bounding polygon from numpy array using quick hull algorithm
    hull = json.dumps(quickHull(arr), cls=NumpyAwareJSONEncoder)
    # Add 'quckhull' polygon to metadata
    metadata["preQuickHull"] = hull
    metadata["filename"] = filepath.split('/')[-1:][0]
    # Write metadata to metafile:
    with open(metafile, "w") as f:
        f.write(json.dumps(metadata, ensure_ascii=False))
    return metadata


@shared_task
def postHullMetadata(result, filepath, tag):
    """Add, from result array, additional metadata to metafile after
    file parsing"""
    # Return false if given result contains no data
    if not result['result']:
        return False

    # Define convenient variables from result:
    # model = result['model']
    output = result['output']
    points = int(result['points'])

    # Reading metadata from existing metafile:
    metafile = '%s.%s.meta' % (filepath, tag)
    # Define metadata dictionary
    metadata = {}
    # Read metadata if metafile exists:
    if os.path.exists(metafile):
        with open(metafile) as json_data:
            metadata = json.load(json_data)

    # print('result %s' % (result))
    # print('metafile %s' % (metafile))

    # Trying to create reslut hull
    try:
        # Load dataset from file to numpy array
        dataset = np.loadtxt(output)
        # Remove third (values) axis from numpy array
        dataset = np.delete(dataset, 2, axis=1)
        # Create bounding polygon from numpy array using quick hull algorithm
        hull = json.dumps(quickHull(dataset), cls=NumpyAwareJSONEncoder)
    # Return empty array if creating quick hull failed
    except Exception:
        hull = []

    # Add number of parsed points to metadata
    metadata["points"] = points
    # Add data type tag to metadata
    metadata["tag"] = tag
    # Add 'quckhull' polygon to metadata
    metadata["quickHull"] = hull
    metadata["filename"] = filepath.split('/')[-1:][0]

    # Write metadata to metafile
    with open(metafile, "w") as f:
        f.write(json.dumps(metadata, ensure_ascii=False))

    return metadata



# Get metadata from NetCDF file before file parsing, write metadata to metafile
# and return metadata for futher processing
@shared_task
def preNetCDFMetadata(filepath, tag):
    # Path to metafile
    metafile =  '%s.%s.meta' % (filepath, tag)

    # Extract metadata from NetCDF
    try:
        metadata = getNetCDFMetaData(filepath)
    except:
        return False


    # Write metadata to metafile
    with open(metafile, "a") as f:
        f.write(json.dumps(metadata, ensure_ascii=False))

    return metadata
