from celery.utils.log import get_logger
import pandas as pd
#
from app.settings import Cache
#
logger = get_logger(__name__)


# Parsing csv file for (in order): latitudes, longitudes, dates;
# store restul at Memcached server
def parseCSV(filepath):
    """Parsing CSV files"""
    logger.info("parsing CSV file: %s" % (filepath))
    # Pointer to the dataset that will be stored at the cache server
    dataset = filepath
    points = 0
    # Array of parsed points from .csv file
    data = []

    df = pd.read_csv(filepath)
    reader = df[['Date', 'Lat', 'Lon']]
    for k in range(len(reader)):
        row = reader[k:k+1]
        date = row['Date'] + ' 00:00:00'
        date = '2012-08-10 00:00:00'
        data.append({
            'lat': float(row['Lat']),
            'lon': float(row['Lon']),
            'datetime': date,
            'value': float(1),
        })
        points += 1

    # Store data at Memcached server
    Cache.set(dataset, data)

    # Return parsed data using pointer, return metadata
    if points > 0:
        return {
                    "dataset": dataset,
                    "metadata": {
                        "points": points,
                    }
                }
    else:
        return False
