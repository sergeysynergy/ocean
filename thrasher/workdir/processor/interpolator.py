from scipy.interpolate import griddata
import numpy as np
import logging
import os
#
from app.settings import TESTMODE, GRIDS


logger = logging.getLogger(__name__)
# logging.basicConfig(level=logging.DEBUG)
outputdir = GRIDS


def getRegionGrid(regionSlug):
    if TESTMODE:  # 4 testing
        regionSlug = 'test-' + regionSlug
    # Path to file with grid array
    filepath = '%s/%s.csv' % (outputdir, regionSlug)
    # Load numpy array from CSV file
    grid = np.loadtxt(filepath)

    # Extract data from numpy array
    gridLats = grid[:, 0]
    gridLons = grid[:, 1]

    return gridLons, gridLats


def interpolateArrayToGrid(arr, gridLons,  gridLats):
    """Interpolate NumPy 2d array to grid, made from gridLons and gridLats"""
    # Extract data from numpy array as needed for futher interpolation:
    lats = arr[0:, 0]
    lons = arr[0:, 1]
    values = arr[0:, 2]
    # Join lats/lons points in one array for futher interpolation
    points = np.column_stack((lats, lons))
    # points = np.random.rand(points.shape[0], 2)  # 4 testing
    # Make interpolation using gridLats, gridLons and numpy algorithm
    gridValues = griddata(
        points, values, (gridLons, gridLats), method='nearest')
    # logger.warning("GIRID %s" % gridValues)
    # Collect latitudes, longitudes and values into one numpy array
    result = np.column_stack((
        gridLats.ravel(), gridLons.ravel(), gridValues.ravel()))

    return result


def interpolateByDay(filesList, regionSlug, outputdir, filename):
    """Make interpolations for the 'ByDay' process"""

    fyles = []
    for fyle in filesList:
        # Load data from file into numpy array
        arr = np.loadtxt(fyle)
        # Checking numpy array shape for one row output
        if len(arr.shape) == 1:
            arr = np.array([arr])
        # Check if any data have been loaded from file, then append it to list
        if arr.shape[0] > 0:
            fyles.append(arr)

    # EXIT CONDITION
    # Concatenate all data from files in list into one numpy array
    if len(fyles) > 0:
        dataset = np.concatenate((fyles))
    else:
        return False

    gridLons, gridLats = getRegionGrid(regionSlug)
    result = interpolateArrayToGrid(dataset, gridLons, gridLats)
    np.savetxt('%s/%s.csv' % (outputdir, filename), result)

    return True


def interpolateBySet(dataset, regionSlug, outputdir, filename):
    """Make interpolations for the 'BySet' process"""
    gridLons, gridLats = getRegionGrid(regionSlug)
    result = interpolateArrayToGrid(dataset, gridLons, gridLats)
    # Prepare output directory:
    if not os.path.exists(outputdir):
        os.makedirs(outputdir)
    np.savetxt('%s/%s.csv' % (outputdir, filename), result)

    return True
