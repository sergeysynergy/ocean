#!/bin/bash

if [[ $1 = "init" ]]; then

  exit 1
fi

if [[ $1 = "dev" ]]; then
  python manage.py runserver 0.0.0.0:8000
  exit 1
fi

if [[ $1 = "test" ]]; then
  DJANGO_ENV=test python manage.py runserver 0.0.0.0:8000

  exit 1
fi

if [[ $1 = "prod" ]]; then
  DJANGO_ENV=production python manage.py runserver 0.0.0.0:8000

  exit 1
fi

if [[ $1 = "migrate" ]]; then
  python manage.py makemigrations
  python manage.py makemigrations ocean
  python manage.py makemigrations chlorophyll
  python manage.py makemigrations sst
  python manage.py makemigrations kamikadze
  python manage.py makemigrations trusted

  python manage.py migrate --database=default
  python manage.py migrate --database=oceandb
  python manage.py migrate --database=chlorophyll
  python manage.py migrate --database=sst
  python manage.py migrate --database=kamikadze
  python manage.py migrate --database=trusted

  python manage.py graphql_schema --indent 2
  mv schema.json /data/.

  exit 1
fi

if [[ $1 = "schema" ]]; then
  clear
  python manage.py graphql_schema --indent 2
  mv schema.json /data/.
  exit 1
fi

if [[ $1 = "flower" ]]; then
  celery -A app flower
  exit 1
fi

if [[ $1 = "jupyter" ]]; then
  jupyter notebook --ip 0.0.0.0 --allow-root
  exit 1
fi
################################################################

if [[ $1 = "cpipe" ]]; then
  celery -A app beat &
  celery -A app worker --loglevel=info -Q parser &
  celery -A app worker --loglevel=info -Q django
  exit 1
fi

if [[ $1 = "cbeat" ]]; then
  # celery -A main beat
  clear
  celery -A app beat

  exit 1
fi

if [[ $1 = "celery" ]]; then
  clear
  celery -A app worker --loglevel=info
  exit 1
fi

if [[ $1 = "cparser" ]]; then
  clear
  celery -A app worker --loglevel=info -Q parser -n parser
  # celery -A app worker -Q parser -n parser
  exit 1
fi

if [[ $1 = "cprocessor" ]]; then
  clear
  celery -A app worker --loglevel=info -Q processor -n processor
  exit 1
fi

if [[ $1 = "cdjango" ]]; then
  clear
  celery -A app worker --loglevel=info -Q django -n django
  exit 1
fi

if [[ $1 = "cpurge" ]]; then
  clear
  celery -A app purge -f
  celery -A app purge -Q parser,django,processor -f
  celery -A app purge -Q parser,django,processor -f
  celery -A app purge -Q parser,django,processor -f
  exit 1
fi

# Default command without arguments
python manage.py runserver 0.0.0.0:8000
