from __future__ import absolute_import, unicode_literals
from celery import shared_task
import logging
#
from app import celeryApp
from app.settings import BEAT_RATE
# from app.celery import debug_task
# from chlorophyll.tasks import hm
# import chlorophyll
logger = logging.getLogger(__name__)
# logging.basicConfig(level=logging.DEBUG)
#
logger.info("BEAT TASKS RATE: %s sec."%(BEAT_RATE))

# Config periodic task execution
celeryApp.conf.beat_schedule = {
    'repeatly-run-processfileswithinsert': {
        'task': 'processor.processfileswithinsert.processFilesWithInsert',
        'schedule': BEAT_RATE,
        'args': (),
        'options': {'queue': 'parser'},
    },
}
"""
    'repeatly-parse-chlorophyll-points': {
        'task': 'parseChlorophyll',
        'schedule': 5.0,
        'args': (),
        'options': {'queue' : 'parser'},
    },
    'repeatly-beat-test': {
        'task': 'beat.tasks.test',
        'schedule': BEAT_RATE,
        'args': (),
        'options': {'queue' : 'parser'},
    },
    'repeatly-chlorophyll-test': {
        'task': 'chlorophyll.tasks.test',
        'schedule': BEAT_RATE,
        'args': ([10,2]),
        'options': {'queue' : 'parser'},
    },
"""
