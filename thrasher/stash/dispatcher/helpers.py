from scipy.interpolate import griddata
import numpy as np
import logging
import os
#
from app.settings import TESTMODE
#
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


def interpolate(filesList, outputPath):
    fyles = []
    for fyle in filesList:
        # Load data from file into numpy array
        arr = np.loadtxt(fyle)
        # Check if any data have been loaded from file, then append it to list
        if arr.shape[0] > 0:
            fyles.append(arr)
    # Concatenate all data from files in list into one numpy array
    if len(fyles) > 0:
        dataset = np.concatenate((fyles))
    else:
        return False

    # Extract data from numpy array as needed for futher interpolation:
    lats = dataset[0:, 0]
    lons = dataset[0:, 1]
    vals = dataset[0:, 2]
    points = np.column_stack((lats, lons))

    """  not needed anymore?
    # Do dimensions compatibility check:
    if points.shape[0] != vals.shape[0]:
        logger.info('Dimensions not match for files: %s' % (filesList))
        return False
    """

    # NorvSea interpolation
    if TESTMODE:  # 4 testing
        glats = np.linspace(60, 70, 100)
        glons = np.linspace(-15, 15, 200)
    else:
        glats = np.linspace(59.45, 77.78, 1652)
        glons = np.linspace(-15.74, 27.96, 2035)
    vlons, vlats = np.meshgrid(glons, glats)
    grid = griddata(points, vals, (vlons, vlats), method='nearest')
    result = np.column_stack((vlats.ravel(), vlons.ravel(), grid.ravel()))
    np.savetxt('%s/norvsea.csv' % (outputPath), result)

    # Kurils interpolation
    if TESTMODE:  # 4 testing
        glats = np.linspace(40, 41, 10)
        glons = np.linspace(145, 150, 20)
    else:
        glats = np.linspace(39.98, 43.90, 436)
        glons = np.linspace(145.93, 151.67, 449)
    vlons, vlats = np.meshgrid(glons, glats)
    grid = griddata(points, vals, (vlons, vlats), method='nearest')
    result = np.column_stack((vlats.ravel(), vlons.ravel(), grid.ravel()))
    np.savetxt('%s/kurils.csv' % (outputPath), result)

    return True


def interpolateArray(arr, outputPath):
    """Interpolate to grids and save to file data from numpy array with:
    latitudes, longitudes and values
    """
    # Extract data from numpy array as needed for futher interpolation:
    lats = arr[0:, 0]
    lons = arr[0:, 1]
    values = arr[0:, 2]
    points = np.column_stack((lats, lons))

    # NorvSea interpolation
    if TESTMODE:  # 4 testing
        glats = np.linspace(60, 70, 100)
        glons = np.linspace(-15, 15, 200)
    else:
        glats = np.linspace(59.45, 77.78, 1652)
        glons = np.linspace(-15.74, 27.96, 2035)
    vlons, vlats = np.meshgrid(glons, glats)
    grid = griddata(points, values, (vlons, vlats), method='nearest')
    result = np.column_stack((vlats.ravel(), vlons.ravel(), grid.ravel()))

    # Save results to file, create directory if needed:
    filename = '%s/norvsea.csv' % (outputPath)
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    np.savetxt(filename, result)

    # Kurils interpolation
    if TESTMODE:  # 4 testing
        glats = np.linspace(40, 41, 10)
        glons = np.linspace(145, 150, 20)
    else:
        glats = np.linspace(39.98, 43.90, 436)
        glons = np.linspace(145.93, 151.67, 449)
    vlons, vlats = np.meshgrid(glons, glats)
    grid = griddata(points, values, (vlons, vlats), method='nearest')
    result = np.column_stack((vlats.ravel(), vlons.ravel(), grid.ravel()))
    np.savetxt('%s/kurils.csv' % (outputPath), result)

    return True


# interpolateArrayToGrid(arr, grid) return result
