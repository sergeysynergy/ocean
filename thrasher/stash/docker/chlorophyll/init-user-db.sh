#!/bin/bash

# set -e

# pg_dump -U postgres port > /backup/port_init.20161206
# psql -U postgres -f ./postgis/backup/port_init postgres

    # CREATE DATABASE chlorophyll TEMPLATE='template_postgis';
    # CREATE USER django WITH PASSWORD 'Passw0rd33';
    # GRANT ALL PRIVILEGES ON DATABASE chlorophyll TO django;
su postgres
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE DATABASE chlorophyll TEMPLATE='template1';
EOSQL
