amqp==2.2.2
Babel==2.6.0
billiard==3.5.0.3
celery==4.1.0
Django==2.0.3
django-cors-headers==2.2.0
django-environ==0.4.4
django-filter==1.1.0
django-graphql-jwt==0.1.8
flower==0.9.2
# GDAL==2.2.2
iso8601==0.1.12
kombu==4.1.0
Pillow==5.0.0
promise==2.1
psycopg2-binary==2.7.4
pycurl==7.43.0
pygobject==3.20.0
PyJWT==1.6.1
pymemcache==1.4.4
python-apt
python-dateutil==2.7.3
pytz==2018.3
redis==2.10.6
Rx==1.6.1
singledispatch==3.4.0.3
six==1.11.0
tornado==5.0.2
typing==3.6.4
unattended-upgrades==0.1
vine==1.1.4
jupyter
regionmask
scipy
# matplotlib
numpy
graphene
graphene-django
graphql-core
graphql-relay
netCDF4
pandas
h5py
csvmapper
shapely
