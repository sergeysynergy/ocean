import React from 'react'
import { Route } from 'react-router-dom'
import Cookie from 'js-cookie'

// import { FRONT, User } from './includes/Cement'
import MainMenu from './includes/MainMenu'
import { Home } from './Home'
import { Queries } from './Queries'
import { Auth } from './Auth'
import About from './About'


export class MainReact extends React.Component {
  constructor(props) {
    super(props)
    // this.user = new User()
    this.setUser = this.setUser.bind(this)
    // console.log('TOKEN', Cookie.get('roken'))
    this.state = {
      User: {
        token: (Cookie.get('token'))?Cookie.get('token'):null
      }
    }
  }
  componentDidMount() {
    // Cookie.set('name', 'iOne', { expires: 7 })
    // console.log(window.location.hostname, window.location.pathname);
    // Cookie.set('name', 'iOne', { expires: 7 })
    /*
    if (User.Id == -1) {
      if (window.location.pathname != '/auth') {
        window.location = FRONT + '/auth'
      }
    }
    */
  }

  setUser(data) {
    let User = this.state.User
    if (data) {
      // console.log('DATA', data);
      // User.token = data.token
      User.token = 'blabla'
      Cookie.set('token', 'blabla', { expires: 7 })
      this.setState(User: User)
      // window.location = '/'
    } else {
      User.token = null
      Cookie.remove('token')
      this.setState(User: User)
    }
  }

  render() {
    // console.log('user:', this.user.Id);
    // console.log('TYPEOF', typeof this.state.User.token);
    // console.log('TOKEN', this.state.User.token);
    // if (typeof this.state.User.token !== 'undefind') {
    if (this.state.User.token) {
      return (<div>
        <MainMenu />
        <Route exact path="/" component={Home}/>
        <Route path="/queries" component={Queries}/>
        <Route path="/about" component={About}/>
        <Route path="/auth" component={() => (<Auth
          User={this.state.User}
          _setUser={this.setUser}
        />)}/>
      </div>)
    } else {
      return (<div>
        <Auth
          User={this.state.User}
          _setUser={this.setUser}
        />
      </div>)
    }
  }
}
/*
      <MainMenu />
      <Route exact path="/" component={Home}/>
      <Route path="/queries" component={Queries}/>
      <Route path="/about" component={About}/>
      <Route path="/auth" component={Auth}/>
        */
