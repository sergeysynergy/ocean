Group "scan_line_attributes"

variables:
  int year(number_of_lines=2030);
    :long_name = "Scan year";
    :units = "years";
    :_FillValue = -32767; // int
    :valid_min = 1900; // int
    :valid_max = 2100; // int
    :_ChunkSizes = 10U; // uint

  int day(number_of_lines=2030);
    :long_name = "Scan day of year";
    :units = "days";
    :_FillValue = -32767; // int
    :valid_min = 0; // int
    :valid_max = 366; // int
    :_ChunkSizes = 10U; // uint

  int msec(number_of_lines=2030);
    :long_name = "Scan time, milliseconds of day";
    :units = "milliseconds";
    :_FillValue = -32767; // int
    :valid_min = 0; // int
    :valid_max = 86400000; // int
    :_ChunkSizes = 10U; // uint

  byte detnum(number_of_lines=2030);
    :long_name = "Detector Number (zero-based)";
    :_FillValue = -1B; // byte
    :valid_min = 0B; // byte
    :valid_max = 25B; // byte
    :_ChunkSizes = 10U; // uint

  byte mside(number_of_lines=2030);
    :long_name = "Mirror Side (zero-based)";
    :_FillValue = -1B; // byte
    :valid_min = 0B; // byte
    :valid_max = 1B; // byte
    :_ChunkSizes = 10U; // uint

  float slon(number_of_lines=2030);
    :long_name = "Starting Longitude";
    :units = "degree_east";
    :_FillValue = -999.0f; // float
    :valid_min = -180.0f; // float
    :valid_max = 180.0f; // float
    :_ChunkSizes = 10U; // uint

  float clon(number_of_lines=2030);
    :long_name = "Center Longitude";
    :units = "degree_east";
    :_FillValue = -999.0f; // float
    :valid_min = -180.0f; // float
    :valid_max = 180.0f; // float
    :_ChunkSizes = 10U; // uint

  float elon(number_of_lines=2030);
    :_FillValue = -999.0f; // float
    :valid_min = -180.0f; // float
    :valid_max = 180.0f; // float
    :long_name = "Ending Longitude";
    :units = "degree_east";
    :_ChunkSizes = 10U; // uint

  float slat(number_of_lines=2030);
    :valid_min = -90.0f; // float
    :valid_max = 90.0f; // float
    :_FillValue = -999.0f; // float
    :long_name = "Starting Latitude";
    :units = "degree_north";
    :_ChunkSizes = 10U; // uint

  float clat(number_of_lines=2030);
    :long_name = "Center Latitude";
    :units = "degree_north";
    :_FillValue = -999.0f; // float
    :valid_min = -90.0f; // float
    :valid_max = 90.0f; // float
    :_ChunkSizes = 10U; // uint

  float elat(number_of_lines=2030);
    :long_name = "Ending Latitude";
    :units = "degree_north";
    :_FillValue = -999.0f; // float
    :valid_min = -90.0f; // float
    :valid_max = 90.0f; // float
    :_ChunkSizes = 10U; // uint

  float csol_z(number_of_lines=2030);
    :long_name = "Center Solar Zenith Angle";
    :units = "degree";
    :_FillValue = -999.0f; // float
    :valid_min = -90.0f; // float
    :valid_max = 90.0f; // float
    :_ChunkSizes = 10U; // uint
