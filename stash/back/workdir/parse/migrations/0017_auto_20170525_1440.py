# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-25 14:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('parse', '0016_task_path'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='date_start',
            field=models.DateField(blank=True, default=None, null=True, verbose_name='Период, с'),
        ),
    ]
