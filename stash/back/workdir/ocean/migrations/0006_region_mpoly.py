# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-02-21 10:12
from __future__ import unicode_literals

import django.contrib.gis.db.models.fields
import django.contrib.gis.geos.collections
import django.contrib.gis.geos.polygon
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ocean', '0005_remove_region_poly'),
    ]

    operations = [
        migrations.AddField(
            model_name='region',
            name='mpoly',
            field=django.contrib.gis.db.models.fields.MultiPolygonField(default=django.contrib.gis.geos.collections.MultiPolygon(django.contrib.gis.geos.polygon.Polygon(((5.897592, 59.453594), (-15.743695, 64.412255), (15.617894, 77.781198), (27.955703, 70.515584), (5.897592, 59.453594)))), srid=4326),
        ),
    ]
