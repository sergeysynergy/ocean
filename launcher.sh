#!/bin/bash

if [[ $1 = "redev" ]]; then
  docker-compose stop
  ./launcher.sh dev
  exit 1
fi

if [[ $1 = "stage" ]]; then
  # Launch docker claster
  docker-compose up -d
  #
  # BACK module
  # run dev django web-server on backend
  docker exec -d ocean_backdjango ./manage.py runserver 0.0.0.0:8000
  #
  # THRASHER module
  # run dev django web-server on thrasher
  docker exec -d ocean_thrasher_django python manage.py runserver 0.0.0.0:8000
  # run Flow - web based tool for monitoring and administrating Celery clusters
  docker exec -d ocean_thrasher_django celery -A app flower
  # run celery beat task manager demon
  docker exec -d ocean_thrasher_django celery -A app beat
  # run celery parser tasks demon
  docker exec -d ocean_thrasher_django celery -A app worker -Q parser -n parser
  # run celery django tasks demon
  docker exec -d ocean_thrasher_django celery -A app worker -Q django -n django
  # run celery django tasks demon
  docker exec -d ocean_thrasher_django celery -A app worker -Q cooker -n cooker
  #
  # FRONT module
  docker exec -d ocean_front_nodejs yarn start
  #
  # CRABBER module
  docker exec -d ocean_crabberdjango ./manage.py runserver 0.0.0.0:8000
  docker exec -d ocean_crabberdjango celery -A app worker --loglevel=INFO --concurrency=2 -n worker1@%h
  #
  # check of launching execution
  clear
  docker ps|grep thrasher

  exit 1
fi

if [[ $1 = "fm" ]]; then
  # Start browser file manager to access files via browser
  # docker exec -d ocean_thrasher_filemanager node-file-manager -p 5000 -d /result
  docker exec -d ocean_thrasher_filemanager run.sh
  exit 1
fi

if [[ $1 = "jupyter" ]]; then
  # Start jupyter notebook
  docker exec -d ocean_thrasher_django jupyter notebook --ip 0.0.0.0 --allow-root
  exit 1
fi
#################################################################

if [[ $1 = "bulkcopy" ]]; then
  python ./scripts/bulkcopy.py
  exit 1
fi

if [[ $1 = "crabber" ]]; then
  docker exec -d ocean_crabberdjango ./manage.py runserver 0.0.0.0:8000
  docker exec -d ocean_crabberdjango celery -A app worker --loglevel=INFO --concurrency=2 -n worker1@%h
  exit 1
fi

if [[ $1 = "back" ]]; then
  docker exec -d ocean_thrasher_django python manage.py runserver 0.0.0.0:8000
  exit 1
fi

if [[ $1 = "front" ]]; then
  docker exec -d ocean_front_nodejs yarn start
  exit 1
fi

if [[ $1 = "prod" ]]; then
  clear
  docker-compose -f docker-compose.yml -f ./docker-compose.prod.yml up -d

  exit 1
fi

if [[ $1 = "postgis" ]]; then
  clear
  docker run --name ocean_thrasher_chlorophyll \
    -p 21016:5432 \
    -e POSTGRES_PASSWORD=Passw0rd33 \
    -d camptocamp/postgis
    # -d mdillon/postgis
  docker ps | grep postgis

  # docker run -it --link ocean_thrasher_chlorophyll:postgres --rm postgres \
    # sh -c 'exec psql -h "$POSTGRES_PORT_5432_TCP_ADDR" -p "$POSTGRES_PORT_5432_TCP_PORT" -U postgres'

  exit 1
fi

if [[ $1 = "chlorophyllRe" ]]; then
  clear
  docker stop ocean_thrasher_chlorophyll
  docker rm ocean_thrasher_chlorophyll
  docker rmi ocean_thrasher_chlorophyll
  docker-compose --file docker-dev.yml up -d
fi
if [[ $1 = "chlorophyllExec" ]]; then
  docker exec -it  -u postgres ocean_thrasher_chlorophyll psql
  exit 1
fi


if [[ $1 = "thrashedDjangoRe" ]]; then
  clear
  docker stop ocean_thrasher_django
  docker rm ocean_thrasher_django
  docker rmi ocean_thrasher_django
  docker-compose --file docker-dev.yml up -d
  exit 1
fi


if [[ $1 = "rechlor" ]]; then
  rm -rf ./volumes/thrasher/chlorophyll
  cp -rf ./volumes/thrasher/chlorophyll.clean ./volumes/thrasher/chlorophyll
  exit 1
fi

if [[ $1 = "resst" ]]; then
  rm -rf ./volumes/thrasher/sst
  cp -rf ./volumes/thrasher/sst.clean ./volumes/thrasher/sst
  exit 1
fi

# DEFAULT COMMANDS WITHOUT ARGUMENTS
clear
# Start containers:
docker-compose up -d

# Make link to node modules files
docker exec ocean_front_nodejs ln -s /opt/node_modules /workdir/node_modules

# docker exec -d ocean_backdjango ./manage.py runserver 0.0.0.0:8000
docker exec -d ocean_thrasher_django celery -A app flower
# docker exec -d ocean_thrasher_django celery -A app beat

docker exec -d ocean_thrasher_django python manage.py runserver 0.0.0.0:8000
# docker exec -d ocean_thrasher_django celery -A app worker -Q parser -n parser
# docker exec -d ocean_thrasher_django celery -A app worker -Q django -n django

# docker exec -d ocean_front_nodejs yarn start
