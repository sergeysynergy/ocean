#!/bin/bash

git pull
echo " "
git add . -A 
echo " "
DATE=`date +%Y-%m-%d:%H:%M`
# git commit -a -m $DATE
git commit -a -m "Changes to run tests"
echo " "
git push
echo " "
echo "***************************************"
echo "*************** STATUS ****************"
echo "***************************************"
git status
echo " "
