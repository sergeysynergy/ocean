#!/bin/bash
if [[ $1 = "init" ]]; then
  exit 1
fi

if [[ $1 = "dev" ]]; then
  ./manage.py runserver 0.0.0.0:8000
  exit 1
fi

if [[ $1 = "prod" ]]; then
  # DJANGO_ENV=production ./manage.py runserver 0.0.0.0:8000
  /usr/local/bin/uwsgi --ini /workdir/app_uwsgi.ini
  exit 1
fi

if [[ $1 = "migrate" ]]; then
  ./manage.py makemigrations
  ./manage.py migrate
  exit 1
fi

################################################################

if [[ $1 = "celery" ]]; then
  celery -A app worker --loglevel=INFO --concurrency=2 -n worker1@%h
  exit 1
fi

if [[ $1 = "cbeat" ]]; then
  clear
  celery -A app beat
  exit 1
fi

if [[ $1 = "cpurge" ]]; then
  clear
  celery -A app purge -f
  exit 1
fi
